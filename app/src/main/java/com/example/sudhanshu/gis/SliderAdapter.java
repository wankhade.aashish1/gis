package com.example.sudhanshu.gis;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by swapnil on 22/6/18.
 */

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_images={
            R.drawable.eat_icon,
            R.drawable.sleep_icon,
            R.drawable.code_icon
    };

    public String[] slide_headings={
            "ORGANIZATION"
            ,"OUTPUTS",
            "USE"
    };

    public String[] slide_desc= {
            "Govt. of Maharashtra will implement the Wolrd Bank aided project on climate resilient agriculture(PoCRA) in collaboration with IITB.",
            "This application aims to provide farm based values like Water Deficit,Runoff,Soil Mositure,AET,etc. based on minimal user inputs.  ",
            "The application will be of great help to validate the status of the farm from anywhere  " +
                    "and providing adivisory accordingly."

    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==(RelativeLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        return super.instantiateItem(container, position);

        layoutInflater =(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView=(ImageView) view.findViewById(R.id.slideImage);
        TextView slideHeading=(TextView) view.findViewById(R.id.slideheading);
        TextView slideDesc=(TextView) view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDesc.setText(slide_desc[position]);

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
        container.removeView((RelativeLayout) object);
    }
}

