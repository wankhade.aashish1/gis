package com.example.sudhanshu.gis;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.constants.ListAppsActivityContract;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.kml.KmlContainer;
import com.google.maps.android.kml.KmlGeometry;
import com.google.maps.android.kml.KmlLayer;
import com.google.maps.android.kml.KmlPlacemark;
import com.google.maps.android.kml.KmlPolygon;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,LocationListener {
    private static final String fine_location = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String course_location = Manifest.permission.ACCESS_COARSE_LOCATION;
    private Boolean permissionGranted = false;
    Button button;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ImageView gps;
    Double lat, longi;
    private EditText mSearchText;
    GoogleApiClient mGoogleApiClient;
    LocationManager locationManager;
    Location location;
    private ToggleButton ToggleButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        button = findViewById(R.id.button);
//        ToggleButton=(ToggleButton) findViewById(R.id.cadastral);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) == true){
                    Log.e("Create","ifWorked");
                    Toast.makeText(MapActivity.this,"Please Enable GPS",Toast.LENGTH_LONG).show();
                }else {
                    Intent intent = new Intent(MapActivity.this, StartActivity.class);
                    Log.d("Lat",lat.toString());
                    Log.d("Long",longi.toString());
                    intent.putExtra("Latitude", lat.toString());
                    intent.putExtra("Longitude", longi.toString());
                    startActivity(intent);
                }
            }
        });

        mSearchText = findViewById(R.id.input_serach);

        gps = findViewById(R.id.gps);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        getLocationPermissions();

       // Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, MapActivity.class));


    }

    private void init() {

        mSearchText.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Toast.makeText(MapActivity.this, "Entered", Toast.LENGTH_LONG).show();
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        || event.getAction() == KeyEvent.KEYCODE_ENTER) {

                    geoLocate();
                }
                return false;
            }
        });
        gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });
    }

    private void geoLocate() {
        String searchString = mSearchText.getText().toString();
        Geocoder geocoder = new Geocoder(MapActivity.this);
        List<Address> list = new ArrayList<>();

        try {
            list = geocoder.getFromLocationName(searchString, 1);
        } catch (IOException e) {
            Log.e("TAG", "geoLocate: " + e.getMessage());
        }

        if (list.size() > 0) {
            Address address = list.get(0);
            Log.d("TAG", "Found a location: " + address.toString());
            Log.d("Lat", String.valueOf(address.getLatitude()));
            Log.d("Long", String.valueOf(address.getLongitude()));
            moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), 15f, address.getAddressLine(0));
        }
    }

    private void getDeviceLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (permissionGranted) {
                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = (Location) task.getResult();
                            Log.d("Location info", String.valueOf(currentLocation));
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15f, "My Location");
                        } else {
                            Toast.makeText(MapActivity.this, "Unable to get current location", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

        } catch (SecurityException e) {
            Log.e("TAG", e.getMessage());
        }
    }

    private void moveCamera(LatLng latLng, float zoom, final String title) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        MarkerOptions options = new MarkerOptions().position(latLng).title(title).draggable(true);

        mMap.addMarker(options);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mMap.clear();
//                Toast.makeText(MapActivity.this, cameraPosition.target.toString(), Toast.LENGTH_LONG).show();
                MarkerOptions marker = new MarkerOptions().position(cameraPosition.target).title(title);
                mMap.addMarker(marker);

                lat = cameraPosition.target.latitude;
                longi = cameraPosition.target.longitude;




            }
        });

    }

    private void initMap() {
        Log.d("InitMap", "InitMap");
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true){
            Log.e("Create","ifWorked");
            Toast.makeText(MapActivity.this,"Please Enable GPS",Toast.LENGTH_LONG).show();
        }else{
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(MapActivity.this);


                     //Added for statellite demo


            init();

        }
    }


    public  void onCadastralAdd(View view){


        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            // Enable vibrate
            Toast.makeText(this, "Cadastral Added 1", Toast.LENGTH_SHORT).show();
            retrieveFileFromResource();
//            retrieveFileFromUrl();
            Toast.makeText(this, "Cadastral Added 2", Toast.LENGTH_SHORT).show();
        } else {
            // Disable vibrate
            Toast.makeText(this, "In else", Toast.LENGTH_SHORT).show();
        }


        }

    private void retrieveFileFromUrl() {
        new DownloadKmlFile(getString(R.string.kml_url)).execute();
    }

    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
        private final String mUrl;

        public DownloadKmlFile(String url) {
            mUrl = url;
        }

        protected byte[] doInBackground(String... params) {
            try {
                InputStream is =  new URL(mUrl).openStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];
                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                return buffer.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(byte[] byteArr) {
            try {
                KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr),
                        getApplicationContext());
                kmlLayer.addLayerToMap();

//                kmlLayer.setOnFeatureClickListener(new KmlLayer.OnFeatureClickListener() {
//                    @Override
//                    public void onFeatureClick(Feature feature) {
//                        Toast.makeText(MapActivity.this,
//                                "Feature clicked: " + feature.getId(),
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });
                moveCameraToKml(kmlLayer);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void retrieveFileFromResource() {
        Toast.makeText(this, "Cadastral Added 3 ", Toast.LENGTH_SHORT).show();
        try {
            KmlLayer kmlLayer = new KmlLayer(mMap, R.raw.campus, getApplicationContext());
            kmlLayer.addLayerToMap();
            Toast.makeText(this, "Layer Added", Toast.LENGTH_SHORT).show();
            moveCameraToKml(kmlLayer);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }



    private void moveCameraToKml(KmlLayer kmlLayer) {
        //Retrieve the first container in the KML layer
        KmlContainer container = kmlLayer.getContainers().iterator().next();
        //Retrieve a nested container within the first container
        container = container.getContainers().iterator().next();
        //Retrieve the first placemark in the nested container
        KmlPlacemark placemark = container.getPlacemarks().iterator().next();
        //Retrieve a polygon object in a placemark
       // Log.d("Geometry -----------",placemark.getGeometry().toString());
       // Toast.makeText(this, ""+placemark.getGeometry(), Toast.LENGTH_SHORT).show();
//        Log.d("Lat -------------",placemark.getProperty("Longitude"));
        KmlPolygon polygon = (KmlPolygon) placemark.getGeometry();
        //Create LatLngBounds of the outer coordinates of the polygon
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polygon.getOuterBoundaryCoordinates()) {
            builder.include(latLng);
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, 1));
    }


    private void getLocationPermissions() {
        Log.d("PErm_MapAct", "Getting location permission");

        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), fine_location) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), course_location) == PackageManager.PERMISSION_GRANTED) {
                permissionGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this, permission, 1234);
            }
        } else {
            ActivityCompat.requestPermissions(this, permission, 1234);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.d("MapAct","Req Map Act");

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionGranted = false;
        switch (requestCode) {
            case 1234: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                            permissionGranted = false;
                            return;
                        }
                    }
                    permissionGranted = true;
                    initMap();

                }
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
//        Toast.makeText(MapActivity.this, "Ready", Toast.LENGTH_LONG).show();
        mMap = googleMap;



        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        RadioGroup rgViews = (RadioGroup) findViewById(R.id.rg_views);

        rgViews.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.rb_normal){
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }else if(checkedId == R.id.rb_satellite){
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
                else if(checkedId == R.id.rb_terrain){
                    mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                }
                else if(checkedId == R.id.rb_hybrid){
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                }

            }
        });


//        loadKml(R.raw.cadastral);
//        try {
//            KmlLayer kmlLayer = new KmlLayer(mMap, R.raw.cadastral, getApplicationContext());
//            kmlLayer.addLayerToMap();
//            moveCameraToKml(kmlLayer);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (XmlPullParserException e) {
//            e.printStackTrace();
//        }

        if (permissionGranted) {
            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            init();
        }
    }


//
//    private void loadKml(File file) {
//
//        try( InputStream inputstream = new FileInputStream(file) ) {
//
//            // Set kmllayer to map
//            // map is a GoogleMap, context is the Activity Context
//
//            KmlLayer layer = new KmlLayer(mMap, inputstream, getApplicationContext());
//
//            layer.addLayerToMap();
//
//
//
//            // Handle these errors
//
//        } catch (FileNotFoundException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        } catch (XmlPullParserException e) {
//
//            e.printStackTrace();
//        }
//
//    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onLocationChanged(Location location) {

    }


}