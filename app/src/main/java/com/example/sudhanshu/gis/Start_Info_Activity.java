package com.example.sudhanshu.gis;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Context;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class Start_Info_Activity extends AppCompatActivity {

    ImageButton IB;
    TextView logo_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start__info_);
        logo_info = (TextView) findViewById(R.id.logo_info);
        logo_info.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/kanak_dynamic.TTF"));
        logo_info.setText("\nशेतीचा ताळेबंद");
        ((ImageView) findViewById(R.id.img1)).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        ((ImageView) findViewById(R.id.img2)).startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Start_Info_Activity.this.startActivity(new Intent(Start_Info_Activity.this, MapActivity.class));
                Start_Info_Activity.this.finish();
            }
        }, 4000);

//         IB = findViewById(R.id.imageButton);
//         logo_info=findViewById(R.id.logo_info);
//
////        TextView text_view = new TextView(this);
////        Typeface font = Typeface.createFromAsset(getAssets(), "/fonts/marathi.ttf");
//        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/kanak_dynamic.TTF");
//        logo_info.setTypeface(typeface);
//        logo_info.setText("शेतीचा ताळेबंद");
//
//
//
//
////        logo_info.setText("It was here");
//
//         IB.setOnClickListener(new View.OnClickListener() {
//             @Override
//             public void onClick(View v) {
//                 Intent intent = new Intent(Start_Info_Activity.this, MapActivity.class);
//                 startActivity(intent);
//             }
//         });
    }




}
