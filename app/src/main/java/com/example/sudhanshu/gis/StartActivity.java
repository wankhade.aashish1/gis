package com.example.sudhanshu.gis;


import org.apache.commons.lang3.StringUtils;

import android.app.ActionBar;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import java.lang.Double;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapDropDown;
import com.beardedhen.androidbootstrap.BootstrapDropDown.OnDropDownItemClickListener;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import fr.ganfra.materialspinner.MaterialSpinner;

public class StartActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView recyclerView1;
    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapter1;
    private List<ListItem> listItems;
    private List<ListItem> listItems1;
    String lat, longi;
    String soil_depth_db,soil_type_db,lulc_type_db,district_db,farmer_name,circle_db, taluka_db;
    //    int detail_output=0;
    Double slope_db;
    HashMap <String,Double [] > rainfall_db;
    HashMap <String,Double [] > rainfall_daily;

    boolean [] dates_check;
    List<String> irr_dates = new ArrayList<String>(10); // initial size


    String soiltype[] = {"Select Soil Type","Clay loam", "Clayey", "Gravelly clay", "Gravelly clay loam", "Gravelly loam", "Gravelly sandy clay loam", "Gravelly sandy loam", "Gravelly silty clay", "Gravelly silty loam", "Loamy","Loamy Sand","Sandy","Sandy clay", "Sandy clay loam", "Sandy loam","Silty clay", "Silty clay loam","Silty loam","Habitation Mask","Waterbody Mask"};
    String  landuse[] = {"Select Landuse","Agriculture", "Forest", "Fallow Land", "Habitation", "Scrub", "Waste Land", "Current Fallow"};
    //    String year[] = {"2013", "2014", "2015", "2016", "2017","2018"};
    String year[];
    Integer irrigation[] = {0,1,2,3,4,5,6,7,8,9,10};
    Integer irrigation_rain[] ={0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100};
    String crop[] = {"Soybean","Tur", "Udid", "Maize", "Sorghum","Moong", "Bajri","Cotton", "Onion", "Tomato", "Vegetables", "Chilly", "Sugarcane", "Lemon", "Fodder Crop", "Potato", "Pomogranate", "Banana", "Groundnut", "Brinjal", "Sweetlime", "Small Vegetable", "Sunflower", "Orange", "Turmeric", "Grapes","Rice","Current Fallow Crop","Forest","Wasteland","Scrub","Cauliflower",};
    String soil_depth [] = {"Select Soil Depth","Deep (50 to 100 cm)","Very shallow (< 10 cm)","Deep to very deep (> 50 cm)","Habitation Mask","Shallow (10 to 25 cm)","Waterbody Mask","Moderately deep (25 to 50 cm)","Very deep (> 100 cm)","Shallow to very shallow (< 25 cm)"};
    String[] irrigation_type = {"Flood", "Sprinkler", "Drip", "Furrow", "Other"};
    Integer[] irrigation_type_water = {40, 30, 20, 30, 35};
    BootstrapEditText irrigationwater;
    MaterialSpinner soil_sp,landuse_sp,year_sp,crop_sp,soil_depth_sp,irrigation_water_sp;
    MaterialSpinner irrigation2;
    public String year_sel;
    EditText slope;
    BootstrapEditText monsoonend;
    BootstrapEditText monsoonstart;
    BootstrapEditText yield_real;
    BootstrapButton button;
    BootstrapButton button_dialog;
    BootstrapDropDown bdd;
    BootstrapDropDown bdd_crop;
    BootstrapDropDown bdd_irrigationtype;
    BootstrapDropDown bdd_irrigationwater;
    BootstrapDropDown bdd_landuse;
    BootstrapDropDown bdd_soildepth;
    BootstrapDropDown bdd_year;
    RadioButton modelradioButton;
    RadioGroup radioGroup;

    HashMap<String, Double[]> temp_avg_db;
    HashMap<String, Double[]> temp_max_db;
    HashMap<String, Double[]> temp_min_db;

    HashMap<String, Double[]> temp_avg_db_hourly;
    HashMap<String, Double[]> temp_max_db_hourly;
    HashMap<String, Double[]> temp_min_db_hourly;

    HashMap<String, Double[]> temp_avg_db_daily;
    HashMap<String, Double[]> temp_max_db_daily;
    HashMap<String, Double[]> temp_min_db_daily;

    HashMap<String, Double[]> temp_hrly_avg;
    HashMap<String, Double[]> rh_hrly_avg;
    HashMap<String, Double[]> wind_hrly_avg;
    HashMap<String, Double[]> rainfall_hourly;

    Lookup ll = new Lookup();
    boolean hourly_model = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        final ArrayList<String> y = new ArrayList<>();
        int current_y = Calendar.getInstance().get(Calendar.YEAR);
        int current_m = Calendar.getInstance().get(Calendar.MONTH);
        int current_d = Calendar.getInstance().get(Calendar.DATE);
        if (current_m < 6 && current_d < 31) {
            current_y--;
        }
        for(int i=2013;i<=current_y;i++){
            y.add(Integer.toString(i));
        }
        year = new String[y.size()];
        year = y.toArray(year);
        lat = getIntent().getExtras().get("Latitude").toString();
        longi = getIntent().getExtras().get("Longitude").toString();
        ArrayAdapter<Integer> arrayAdapter3 = new ArrayAdapter<Integer>(this, android.R.layout.simple_dropdown_item_1line, irrigation);
        irrigation2 = findViewById(R.id.irrigation);
        irrigation2.setAdapter(arrayAdapter3);

        radioGroup=(RadioGroup)findViewById(R.id.modelgroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if(radioButton==null) {
                    hourly_model=true;
                    return;
                }
                Toast.makeText(getApplicationContext(), radioButton.getText().toString(), Toast.LENGTH_LONG).show();
                hourly_model = radioButton.getText().toString().equals("ताशी मॉडेल");
                Log.d("modelselection", radioButton.getText().toString());
                if(hourly_model){
                    String item = irrigation2.getSelectedItem().toString();
                    int n = Integer.parseInt(item);
                    for(int i1=0;i1<n;i1++){
                        View v = recyclerView1.getLayoutManager().findViewByPosition(i1);
                        View date_v = recyclerView.getLayoutManager().findViewByPosition(i1);
                        int padding_in_dp = 315;  // 6 dps
                        final float scale = getResources().getDisplayMetrics().density;
                        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                        try {
                            v.findViewById(R.id.water_hours).setVisibility(View.VISIBLE);
                            v.findViewById(R.id.spacing_along).setVisibility(View.VISIBLE);
                            v.findViewById(R.id.spacing_between).setVisibility(View.VISIBLE);
                            v.findViewById(R.id.total_area).setVisibility(View.VISIBLE);
                            date_v.findViewById(R.id.date).setPadding(0, 0, 0, padding_in_px);
                        }catch (Exception e){

                        }
                    }


                }else{
                    String item = irrigation2.getSelectedItem().toString();
                    int n = Integer.parseInt(item);
                    for(int i1=0;i1<n;i1++){
                        View v = recyclerView1.getLayoutManager().findViewByPosition(i1);
                        View date_v = recyclerView.getLayoutManager().findViewByPosition(i1);
                        int padding_in_dp = 230;
                        final float scale = getResources().getDisplayMetrics().density;
                        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
                        try {
                            v.findViewById(R.id.water_hours).setVisibility(View.GONE);
//                            v.findViewById(R.id.spacing_along).setVisibility(View.GONE);
//                            v.findViewById(R.id.spacing_between).setVisibility(View.GONE);
                            v.findViewById(R.id.total_area).setVisibility(View.GONE);
                            date_v.findViewById(R.id.date).setPadding(0, 0, 0, padding_in_px);
                        }catch (Exception e){

                        }

                    }

                }
            }
        });

        button = StartActivity.this.findViewById(R.id.buttonRun);



        final CheckBox myCheckBox= findViewById(R.id.item_check);
        final CheckBox terracingcheckbox=(CheckBox) findViewById(R.id.terracing_check);
        final EditText fname=(EditText) findViewById(R.id.farmer_name);
        final boolean isChecked = myCheckBox.isChecked();
        final boolean isTerracingChecked = terracingcheckbox.isChecked();
        monsoonend = findViewById(R.id.monsoonend);
        monsoonstart = findViewById(R.id.monsoonstart);
        yield_real = findViewById(R.id.yield_real);
//        final RadioGroup rg = (RadioGroup) findViewById(R.id.rg_display);
//        final int[] selectedId = {rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()))};
//        Toast.makeText(StartActivity.this, lat + " " + longi, Toast.LENGTH_LONG).show();


//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Farmwater");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, soiltype);
        bdd = findViewById(R.id.soiltype_1);
        bdd.setDropdownData(soiltype);
        bdd.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                Toast.makeText(StartActivity.this, soiltype[id], Toast.LENGTH_LONG).show();
                bdd.setText(StartActivity.this.soiltype[id]);
            }
        });
//        soil_sp.setAdapter(arrayAdapter);

//        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, landuse);
//        landuse_sp = findViewById(R.id.landuse);
//        landuse_sp.setAdapter(arrayAdapter1);
        bdd_landuse = findViewById(R.id.landuse);
        bdd_landuse.setDropdownData(landuse);
        bdd_landuse.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            public void onItemClick(ViewGroup parent, View v, int id) {
                bdd_landuse.setText(landuse[id]);
            }
        });

//        final ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, year);
//        year_sp = findViewById(R.id.year);
//        year_sp.setAdapter(arrayAdapter2);
        bdd_year = findViewById(R.id.year);
        bdd_year.setDropdownData(year);
        SharedPreferences.Editor edit = getSharedPreferences("year_selected", 0).edit();
        edit.putString("year", "2019");
        edit.apply();
        bdd_year.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            public void onItemClick(ViewGroup parent, View v, int id) {
                bdd_year.setText(year[id]);
                year_sel = bdd_year.getText().toString();
                SharedPreferences.Editor edit = getSharedPreferences("year_selected", 0).edit();
                edit.putString("year", year_sel);
                edit.apply();
                String sb = "10/10/"+year_sel;
                monsoonend.setText(sb);
                monsoonstart.setText("15/06/"+year_sel);
                LinearLayout model_ll = findViewById(R.id.model_ll);
                if(Integer.parseInt(year_sel)>=2018){
                    model_ll.setVisibility(View.VISIBLE);
                }else{
                    modelradioButton = findViewById(R.id.daily_model_radio);
                    modelradioButton.setChecked(true);
                    model_ll.setVisibility(View.GONE);
                }



            }
        });




//        ArrayAdapter<String> arrayAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, crop);
//        crop_sp = findViewById(R.id.crop);
//        crop_sp.setAdapter(arrayAdapter4);
        bdd_crop = findViewById(R.id.crop);
        bdd_crop.setDropdownData(crop);
        bdd_crop.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            public void onItemClick(ViewGroup parent, View v, int id) {
                bdd_crop.setText(crop[id]);
            }
        });

//        ArrayAdapter <String> arrayAdapter5 = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,soil_depth);
//        soil_depth_sp = findViewById(R.id.soildepth);
//        soil_depth_sp.setAdapter(arrayAdapter5);

        bdd_soildepth = findViewById(R.id.soildepth_1);
        bdd_soildepth.setDropdownData(soil_depth);
        bdd_soildepth.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            public void onItemClick(ViewGroup parent, View v, int id) {
                bdd_soildepth.setText(soil_depth[id]);


            }
        });

//        final ArrayAdapter<Integer> arrayAdapter6 = new ArrayAdapter<Integer>(this, android.R.layout.simple_dropdown_item_1line, irrigation_rain);
//        irrigation_water_sp = findViewById(R.id.irrigation_water);
//        irrigation_water_sp.setAdapter(arrayAdapter6);
//        irrigation_water_sp.setSelection(35);



        slope = findViewById(R.id.slope);
        getData(lat, longi);

//        setData();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Lookup ll = new Lookup();

                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radio button by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if(radioButton == null){
                    Toast.makeText(getApplicationContext(), "Please select daily/hourly model", Toast.LENGTH_LONG).show();
                    return;
                }

                String soil_sel,crop_sel,soil_dep_sel;
                String lulc_sel2 = bdd_landuse.getText().toString();
                soil_sel = bdd.getText().toString();
                crop_sel = bdd_crop.getText().toString();
                soil_dep_sel = bdd_soildepth.getText().toString();
                String monsoon_start_str = monsoonstart.getText().toString();
                boolean flag = true;
                if (!StartActivity.isValidDate(monsoon_start_str)) {
                    Toast.makeText(StartActivity.this, "मॉन्सून सुरुवात दिन निवडा", Toast.LENGTH_LONG).show();
                    flag = false;
                }
                if(fname.getText().toString().trim().length() == 0)
                {
                    //Toast.makeText(StartActivity.this,"Add Farmer Name",Toast.LENGTH_LONG).show();
                    Toast.makeText(StartActivity.this,"शेतकरी नाव जोडा",Toast.LENGTH_LONG).show();
                    flag =false;
                }
                if(lulc_sel2.equals("Select Landuse") || lulc_sel2.equals("जमिनीचा वापर निवडा")){
                    // Toast.makeText(StartActivity.this,"Select Landuse",Toast.LENGTH_LONG).show();
                    Toast.makeText(StartActivity.this,"जमीन वापर निवडा",Toast.LENGTH_LONG).show();
                    flag =false;
                }
                if(soil_sel.equals("Select Soil Type") || soil_sel.equals("मातीचा प्रकार")){
                    //Toast.makeText(StartActivity.this,"Select Soil Type",Toast.LENGTH_LONG).show();
                    Toast.makeText(StartActivity.this, "माती प्रकार निवडा",Toast.LENGTH_LONG).show();
                    flag =false;
                }
                if(soil_dep_sel.equals("Select Soil Depth") || soil_dep_sel.equals("मातीचा खोलता निवडा")){
                    //Toast.makeText(StartActivity.this,"Select Soil Depth",Toast.LENGTH_LONG).show();
                    Toast.makeText(StartActivity.this,"मातीची खोली निवडा",Toast.LENGTH_LONG).show();
                    flag =false;
                }


                if(flag){

//                final ProgressDialog progressDialog = new ProgressDialog(StartActivity.this);
//                progressDialog.setMax(100);
//                progressDialog.setMessage("Please Wait...");
//                progressDialog.setTitle("Copying contents to file..");
//
//
//
//                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                progressDialog.show();
//
//
//                    final Handler handle = new Handler() {
//                        @Override
//                        public void handleMessage(Message msg) {
//                            super.handleMessage(msg);
//                            progressDialog.incrementProgressBy(1);
//                        }
//                    };
//
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                while (progressDialog.getProgress() <= progressDialog
//                                        .getMax()) {
//                                    Thread.sleep(200);
//                                    handle.sendMessage(handle.obtainMessage());
//                                    if (progressDialog.getProgress() == progressDialog
//                                            .getMax()) {
//                                        progressDialog.dismiss();
//                                    }
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }).start();



                    int sowing_threshold = 30;
                    double [] rf_plus_irrg;
                    double [] rainfall;
                    boolean hourly_model = radioButton.getText().toString().equals("ताशी मॉडेल");

                    Log.d("hourly_model_value", ""+hourly_model);
                    if(hourly_model){
                        rf_plus_irrg = dlbTodouble(rainfall_hourly.get(year_sel));
                        rainfall = dlbTodouble(rainfall_hourly.get(year_sel));
                    }else{
                        rf_plus_irrg=dlbTodouble(rainfall_daily.get(year_sel));
                        rainfall = dlbTodouble(rainfall_daily.get(year_sel));
                    }

//                double [] et = {7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 7.51, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.77, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.55, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.78, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.9, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.48, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 3.95, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.17, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 5.29, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 7.03, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25, 8.25};
                    double [] et;
                    String et_key = year_sel+"_"+district_db+"_"+taluka_db+"_"+circle_db;
                    if(ll.dynamic_et_daily.containsKey(et_key) && !hourly_model){
                        et = (double[]) ll.dynamic_et_daily.get(et_key);
                        Toast.makeText(getApplicationContext(), "Dynamic ET0 Taken", Toast.LENGTH_LONG).show();
                    }
                    else if(ll.dynamic_et_hourly.containsKey(et_key) && hourly_model){
                        et = (double[]) ll.dynamic_et_hourly.get(et_key);
                        Toast.makeText(getApplicationContext(), "Dynamic HOURLY ET0 Taken", Toast.LENGTH_LONG).show();
                        Log.i("model", "Dynamic hourly et0");
                    }
                    else if(ll.et.containsKey(district_db) ){
                        et = ll.et.get(district_db);
                    }else
                        et = ll.et.get("Default");

                    int monsoon_end_index = (int) getDifferenceDays( monsoonend.getText().toString(), year_sel);
//                Log.d("Monsoon end index", Integer.toString(monsoon_end_index));
//                int irrigation_water_mm = (int) irrigation_water_sp.getSelectedItem();
//                Log.d("TAG IRRG",Integer.toString(irrigation_water_mm));
                    double [] irrgation = new double[365];
                    double[] soilmoisture = new double[365];
                    String item = irrigation2.getSelectedItem().toString();
                    int n = Integer.parseInt(item);
                    irr_dates.clear();
                    String fortoast = "";
                    String per_irrigation_water = "";

//                Log.d("No of times", Integer.toString(n));
                    String water_computed[] = new String[n];
                    for (int i=0;i<n;i++){
                        SharedPreferences spfr = getSharedPreferences("dates", 0);
                        String dt = spfr.getString("Date_"+Integer.toString(i),"none");
                        irr_dates.add(dt);
                        String water = spfr.getString("Water_"+Integer.toString(i), "none");
                        String type = spfr.getString("Type_"+Integer.toString(i), "none");
                        int hrs = 1;
                        double yeild=0.0;
                        double  area = 0.0;
                        int num_of_days = 1;
                        double spacing_along = 1;
                        double spacing_between = 2;
                        double num_of_drippers_or_sprinklers = 1;
                        double irr_watering = 0;
                        if(hourly_model) {
                            hrs = Integer.parseInt(spfr.getString("Waterhours_" + Integer.toString(i), "none"));
                            area = Double.parseDouble(spfr.getString("Area_"+i, "none"));
                            area *= 4046; //converting to m2
                            num_of_days = Integer.parseInt(spfr.getString("Days_"+i, "none"));
                            if(type.equals("Drip")){
                                spacing_along = Double.parseDouble(spfr.getString("SpacingAlong_"+i, "none"));
                                spacing_between = Double.parseDouble(spfr.getString("SpacingBetween_"+i, "none"));
                                double num_of_drippers = 4046 / (spacing_along*spacing_between);
                                irr_watering = (num_of_drippers * Double.parseDouble(water) * hrs * 0.9)/4046;
                                water = String.valueOf(irr_watering);
                            }
                            else if (type.equals("Sprinkler")){
                                spacing_along = Double.parseDouble(spfr.getString("SpacingAlong_"+i, "none"));
                                spacing_between = Double.parseDouble(spfr.getString("SpacingBetween_"+i, "none"));
                                double num_of_sprinklers = 4046 / (spacing_along*spacing_between);
                                irr_watering = (num_of_sprinklers * Double.parseDouble(water)*hrs*0.75)/4046;
                                water = String.valueOf(irr_watering);
                            }
                            else if (type.equals("Flood")){
                                irr_watering = Double.parseDouble(water)*hrs/4046;
                                water = String.valueOf(irr_watering);
                            }
                            else{
                                water = String.valueOf(Double.parseDouble(water) * hrs/4046);
                            }


                        }else{
                            num_of_days = Integer.parseInt(spfr.getString("Days_"+i, "1"));
                        }
                        water_computed[i] = water;
                        for (int d=0;d<num_of_days;d++)
                            per_irrigation_water += (Double.parseDouble(water))+" ";

//                    Log.d("date",dt);

//                        SharedPreferences.Editor edit = spfr.edit();
//                        try {
//                            edit.remove("Date_" + Integer.toString(i));
//                        }catch (Exception e){}
//                        try {
//                            edit.remove("Water_" + Integer.toString(i));
//                        }catch (Exception e){}
//                        try {
//                            edit.remove("Type_" + Integer.toString(i));
//                        }catch (Exception e){}
//                        edit.apply();


                        int index = (int) getDifferenceDays(dt,year_sel);
                        if(!type.equals("Drip")){
                            for(int d=0;d<num_of_days;d++)
                                irrgation[index+d] = ((double) Double.parseDouble(water));
                            Log.d("Irrigation at"+Integer.toString(index),Double.toString(irrgation[index]));
                            if(!hourly_model)
                                for(int d=0;d<num_of_days;d++)
                                    rf_plus_irrg[index+d] = rf_plus_irrg[index+d] + ((double) Double.parseDouble(water));
                            else{
                                for(int d=0;d<num_of_days;d++){
                                    int index_ = 24*(index+d);
                                    for(int j=0;j<hrs;j++){
                                        ///spreading irrigation over 24 hours
                                        rf_plus_irrg[index_++] += (double) Double.parseDouble(water)/hrs;
                                    }
                                }

                            }
                            //now we need to add day based irrigation into hour based rf


                        }else{
//                            irrgation[index] = rainfall[index];
                            Log.d("error date", dt);
                            for(int d=0;d<num_of_days;d++)
                                soilmoisture[index+d] = soilmoisture[index+d] + ((double) Double.parseDouble(water));
                        }
//                    Log.d("Index"+Integer.toString(i),Integer.toString(index));
//                    irrgation[index]+= rainfall[index]+irrigation_water_mm;
//                    rf_plus_irrg[index]+=irrigation_water_mm;

                    }

//                    Log.d("selected data",lulc_sel+" "+soil_sel.toLowerCase()+" "+ crop_sel+ " "+ year_sel);
//                Log.d("selected data",soil_sel.toLowerCase());
                    double depth_sel =ll.soil_depth.get(soil_dep_sel.toLowerCase());
                    double slope_sel = Double.parseDouble(slope.getText().toString());

                    kharifModel km = new  kharifModel(lulc_sel2.toLowerCase(),soil_sel.toLowerCase(), depth_sel, slope_sel, crop_sel,rainfall,irrgation,monsoon_end_index,year_sel, lat, longi,district_db, soilmoisture );
                    km.setup(hourly_model);
                    km.pET_calculation(et, sowing_threshold, rf_plus_irrg, (int)getDifferenceDays(monsoonstart.getText().toString(),year_sel), hourly_model);

                    if(hourly_model){
                        for (int i=0;i<365;i++) {
                            for(int hr=0;hr<24;hr++){
                                km.primary_runoff(i, hr, rf_plus_irrg);
                                km.Aet(i, hr);
                                km.percolation_below_root_zone(i, hr, soilmoisture[i]);
                                km.secondary_runoff(i, hr);
                                km.percolation_to_GW(i, hr);
                            }
                        }
                    }else{
                        for (int i=0;i<365;i++) {
                            int hr=-1;
                            km.primary_runoff(i, hr, rf_plus_irrg);
                            km.Aet(i, hr);
                            km.percolation_below_root_zone(i, hr, soilmoisture[i]);
                            km.secondary_runoff(i, hr);
                            km.percolation_to_GW(i, hr);

                        }
                    }
//                }

                    km.summerize(hourly_model);
                    Intent intent = new Intent(StartActivity.this,TabResult.class);
                    Gson gson = new Gson();
                    String km_str = gson.toJson(km);
                    // Create SharedPreferences object.
                    Context ctx = getApplicationContext();
                    SharedPreferences sharedPreferences = ctx.getSharedPreferences("km_obj", MODE_PRIVATE);

                    // Put the json format string to SharedPreferences object.
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("km_obj_serial", km_str);
                    editor.commit();



//                    intent.putExtra("serialize data",km_str);
//                Toast.makeText(StartActivity.this, "Instart activty"+ myCheckBox.isChecked(), Toast.LENGTH_SHORT).show();
                    intent.putExtra("checkBoxValue", myCheckBox.isChecked());
                    farmer_name = fname.getText().toString(); //gets you the contents of edit text
                    intent.putExtra("circlename", circle_db.toString());
                    intent.putExtra("talukaname", taluka_db);
                    intent.putExtra("districtname", district_db);
                    intent.putExtra("farmername", farmer_name);
                    intent.putExtra("sowing_offset",km.sowing_date_offset);
                    intent.putExtra("year",year_sel);
//                    intent.putExtra("per_irrigation_water", Integer.toString(irrigation_water_mm));
                    intent.putExtra("per_irrigation_water", per_irrigation_water);
                    intent.putExtra("irrigation_amount",irrigation2.getSelectedItem().toString());
                    intent.putExtra("irrigation_dates", String.valueOf(irr_dates));
                    intent.putExtra("et",et);
                    intent.putExtra("crop_name",crop_sel);
                    intent.putExtra("lat",lat);
                    intent.putExtra("longi",longi);
                    intent.putExtra("watering_computed", water_computed);
                    if(yield_real.getText().toString().trim().length() !=0)
                        intent.putExtra("yield", yield_real.getText().toString());
                    else
                        intent.putExtra("yield", "0");


//                    Log.d("irr_dates", String.valueOf(irr_dates));







//                    intent.putExtra("selected", selectedId[0]);
//                    Log.d("From Start Selected", Integer.toString(selectedId[0]));
//                    Log.d("From Start Selected", String.valueOf(selectedId[0]));
//                    Log.d("PutExtra","Kharif added");
//                progressDialog.dismiss();
                    startActivity(intent);

                }
            }
        });

//        year_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                year_sel = (String) year_sp.getSelectedItem();
//                SharedPreferences spf = getSharedPreferences("year_selected", 0);
//
//                SharedPreferences.Editor edit = spf.edit();
//                edit.putString("year", year_sel);
//                edit.apply();
//
//
//                monsoonend.setText("10/10/"+year_sel);
////                Toast.makeText(StartActivity.this,year_sel,Toast.LENGTH_LONG).show();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        myCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isChecked = myCheckBox.isChecked();
                if(isChecked)
                    Toast.makeText(StartActivity.this, "Detailed Output will take time to display results ", Toast.LENGTH_SHORT).show();
            }
        });

        terracingcheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isTerrChecked) {
                isTerrChecked = terracingcheckbox.isChecked();
                if(isTerrChecked)
                {
                    Toast.makeText(StartActivity.this, "Terracing Done setting Slope to Zero ", Toast.LENGTH_SHORT).show();
                    slope.setText(String.valueOf(0));
                }
                else {
                    slope.setText(slope_db.toString());

                }
            }
        });

//
//        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//            @SuppressLint("WrongConstant")
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//
//                View radioButton = rg.findViewById(checkedId);
//                int index = rg.indexOfChild(radioButton);
//                selectedId[0] = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()));
//
//                // Add logic here
//
//                switch (index) {
//                    case 0: // first button
////                        selectedId[0] = rg.getCheckedRadioButtonId();
//                        Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
//                        break;
//                    case 1: // secondbutton
//
//                        Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
//                        break;
//
//                    case 2: // secondbutton
//
//                        Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
//                        break;
//                }
//            }
//        });

        monsoonend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment d = new DateDialog(v,Integer.parseInt(year_sel),9,10, -1);
                d.show(getFragmentManager(),"Date Picker");

            }
        });
        monsoonstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment d = new DateDialog(v,Integer.parseInt(year_sel),9,10, -1);
                d.show(getFragmentManager(),"Date Picker");

            }
        });

        irrigation2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recyclerView = findViewById(R.id.recyclerView);
                recyclerView1 = findViewById(R.id.recyclerView1);
                String item = irrigation2.getItemAtPosition(position).toString();

                int n = Integer.parseInt(item);
                if (n >= 1) {
                    ((TextView) StartActivity.this.findViewById(R.id.Dateshow)).setText("तारीख");

                } else {
                    ((TextView) StartActivity.this.findViewById(R.id.Dateshow)).setText("");

                }
                for(int i=0;i<n;i++){
                    SharedPreferences spfr = getSharedPreferences("dates", 0);
                    try {
                        SharedPreferences.Editor edit = spfr.edit();
                        try {
                            edit.remove("Date_"+Integer.toString(i));
                            edit.remove("Water_"+Integer.toString(i));
                            edit.remove("Type_"+Integer.toString(i));
                            edit.apply();
                        } catch (Exception e) {
                        }
                    } catch (Exception e2) {
                    }
                }

                recyclerView.setHasFixedSize(true);
                recyclerView1.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(StartActivity.this));
                recyclerView1.setLayoutManager(new LinearLayoutManager(StartActivity.this));

                listItems = new ArrayList<>();
                listItems1 = new ArrayList<>();
                for(int i=0; i < n;i++){
                    ListItem listItem = new ListItem(
                            "Select Date "+Integer.toString(i+1)
                    );
                    listItems.add(listItem);
                }
                for (int i2 = 0; i2 < n; i2++) {
                    listItems1.add(new ListItem("40"));
                }
                adapter = new MyAdapter(listItems, StartActivity.this);
                recyclerView.setAdapter(adapter);
                adapter1 = new IrrigationAdapter(listItems1, StartActivity.this);
                recyclerView1.setAdapter(adapter1);

                radioGroup.clearCheck();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    public void openDialog(){

    }

    public void adjustMethodIrrigationInputs(){
        int selectedId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(selectedId);
        Toast.makeText(getApplicationContext(), radioButton.getText().toString(), Toast.LENGTH_LONG).show();
        boolean hourly_model = radioButton.getText().toString().equals("ताशी मॉडेल");
        if(hourly_model){
            BootstrapEditText water_hours = findViewById(R.id.water_hours);

            String item_ = irrigation2.getSelectedItem().toString();
            int n_ = Integer.parseInt(item_);
            for(int i1=0;i1<n_;i1++){
                View v = recyclerView1.getLayoutManager().findViewByPosition(i1);
                v.findViewById(R.id.water_hours).setVisibility(View.VISIBLE);
            }


        }else{
            String item_ = irrigation2.getSelectedItem().toString();

            int n_ = Integer.parseInt(item_);
            for(int i1=0;i1<n_;i1++){
                View v = recyclerView1.getLayoutManager().findViewByPosition(i1);
                v.findViewById(R.id.water_hours).setVisibility(View.INVISIBLE);
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void itemClicked(View v) {
        //code to check if this checkbox is checked!

    }

    private void setData(){
//        Log.d("Entered set","Setdata");
        Lookup l = new Lookup();
        if(Arrays.asList(soil_depth).indexOf(soil_depth_db) != -1)
            bdd_soildepth.setText(soil_depth[Arrays.asList(soil_depth).indexOf(soil_depth_db)]);
        if(Arrays.asList(soiltype).indexOf(soil_type_db) != -1)
            bdd.setText(soiltype[Arrays.asList(soiltype).indexOf(soil_type_db)]);
        String generalizedLandUse = StringUtils.capitalize(l.landuse.get(lulc_type_db.toLowerCase()));
        if(Arrays.asList(landuse).indexOf(generalizedLandUse) != -1)
            bdd_landuse.setText(landuse[Arrays.asList(landuse).indexOf(generalizedLandUse)]);
//        Toast.makeText(StartActivity.this,slope_db.toString(),Toast.LENGTH_LONG).show();
        slope.setText(slope_db.toString());
//        Log.d("index_soil",soil_depth_db);
//        Log.d("index23",slope_db.toString());
//        Log.d("index_S",soil_depth_db);
//        Log.d("tp",lulc_type_db);
//        Log.d("gpp",generalizedLandUse);
//        Log.d("tpp",Integer.toString(Arrays.asList(landuse).indexOf(generalizedLandUse)));
        year_sel  = getYear_sel();
        monsoonend.setText("10/10/"+year_sel);
        monsoonstart.setText("15/06/"+year_sel);
        String lat_dyn = getIntent().getExtras().get("Latitude").toString();
        String longi_dyn = getIntent().getExtras().get("Longitude").toString();
        int current_year = Calendar.getInstance().get(Calendar.YEAR);
        int current_month = Calendar.getInstance().get(Calendar.MONTH);
        int current_date = Calendar.getInstance().get(Calendar.DATE);
        if (current_month < 6 && current_date < 31) {
            current_year--;
        }
        for (int i=2018;i<=2020;i++){
            String yr = Integer.toString(i);
            Double[] dArr = (Double[]) temp_avg_db_daily.get(yr);
            Double[] dArr2 = (Double[]) temp_min_db_daily.get(yr);
            Double[] dArr3 = (Double[]) temp_max_db_daily.get(yr);


            Double[] dArr2_hourly = (Double[]) temp_min_db_hourly.get(yr);
            Double[] dArr3_hourly = (Double[]) temp_max_db_hourly.get(yr);


            Double[] temp_hrly_avg_ = temp_hrly_avg.get(yr);


            Double[] rh_hrly_avg_ = rh_hrly_avg.get(yr);
            Double[] wind_hrly_avg_ = wind_hrly_avg.get(yr);

//            HashMap<String, Double[]> temp_hrly_avg;
//            HashMap<String, Double[]> rh_hrly_avg;
//            HashMap<String, Double[]> wind_hrly_avg;
//            HashMap<String, Double[]> rainfall_hourly;


            String et_key = yr+"_"+district_db+"_"+taluka_db+"_"+circle_db;
            ll.dynamicEt(dArr, dArr2, dArr3,et_key,Double.valueOf(lat_dyn), Double.valueOf(longi_dyn));

            ll.set_pocra_hourly_et0(Double.valueOf(lat_dyn), Double.valueOf(longi_dyn),dArr3_hourly,dArr2_hourly,temp_hrly_avg_, dArr, rh_hrly_avg_,wind_hrly_avg_,350,0.0, et_key);

        }
//        Log.d("Year Sel",year_sel);

    }


    public String getYear_sel() {
//        return (String) year_sp.getSelectedItem();
        return bdd_year.getText().toString();
    }


    private void  getData(String lat, String longi){
//        String URL = "http://10.129.27.216/test.php?x="+longi+"&y="+lat;
        String URL = "http://gis.mahapocra.gov.in/dashboard_testing_api_2020_09_08/farmapp/get_circle_data?lat="+lat+"&lon="+longi;
//        String URL = "http://www.cse.iitb.ac.in/jaltarang/test1.php?x="+longi+"&y="+lat;
//        String URL = "http://gis.mahapocra.gov.in/dashboard_v2/api/app/?x="+longi+"&y="+lat;

        Log.d("URL",URL);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, URL,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), "Response rcvd", Toast.LENGTH_LONG).show();
                        try {

                            Log.d("JSON",response);
                            JSONObject jsonObj = new JSONObject(response);
                            if(jsonObj.get("soil_depth").equals(null))
                                soil_depth_db = "NULL";
                            else
                                soil_depth_db = (String) jsonObj.get("soil_depth");
                            if(jsonObj.get("soil_type").equals(null))
                                soil_type_db = "NULL";
                            else
                                soil_type_db = (String) jsonObj.get("soil_type");
                            if(jsonObj.get("lulc_type").equals(null))
                                lulc_type_db = "NULL";
                            else
                                lulc_type_db = (String) jsonObj.get("lulc_type");
                            if(jsonObj.get("slope").equals(null))
                                slope_db = 1.0 ;
                            else
                                slope_db = new Double((Double)jsonObj.get("slope"));
//                            Log.d("soil_dep",soil_depth_db);
//                            Log.d("soil_type",soil_type_db);
//                            Log.d("lulc_t",lulc_type_db);
//                            Log.d("slope",slope_db.toString());
                            district_db = (String) jsonObj.get("District");
                            try {
                                circle_db = (String) jsonObj.get("Circle");
                            }catch (Exception e){
                                try{
                                    circle_db = (String) jsonObj.get("SkyCircle");
                                    Log.i("circle", circle_db);
                                }
                                catch (Exception e1){
                                    circle_db = "default";
                                }

                            }
                            taluka_db = (String) jsonObj.get("Taluka");
//                            String[] rf2013 =jsontoString((JSONArray)  jsonObj.get("Rainfall2013"));
//                            String[] rf2014 =jsontoString((JSONArray)  jsonObj.get("Rainfall2014"));
//                            String[] rf2015 =jsontoString((JSONArray)  jsonObj.get("Rainfall2015"));
//                            String[] rf2016 =jsontoString((JSONArray)  jsonObj.get("Rainfall2016"));
//                            String[] rf2017 =jsontoString((JSONArray)  jsonObj.get("Rainfall2017"));
//                            String[] rf2018 =jsontoString((JSONArray)  jsonObj.get("Rainfall2018"));
                            rainfall_db = new HashMap<> ();
                            rainfall_daily = new HashMap<> ();
//                            rainfall_db.put("2013", strtoDouble(rf2013));
//                            rainfall_db.put("2014", strtoDouble(rf2014));
//                            rainfall_db.put("2015", strtoDouble(rf2015));
//                            rainfall_db.put("2016", strtoDouble(rf2016));
//                            rainfall_db.put("2017", strtoDouble(rf2017));
//                            rainfall_db.put("2018", strtoDouble(rf2018));
                            temp_avg_db_daily = new HashMap<>();
                            temp_min_db_daily = new HashMap<>();
                            temp_max_db_daily = new HashMap<>();

                            temp_avg_db_hourly = new HashMap<>();
                            temp_min_db_hourly = new HashMap<>();
                            temp_max_db_hourly = new HashMap<>();

                            temp_hrly_avg = new HashMap<>();
                            rh_hrly_avg = new HashMap<>();
                            wind_hrly_avg = new HashMap<>();
                            rainfall_hourly = new HashMap<>();

                            int current_year = Calendar.getInstance().get(Calendar.YEAR);
                            int current_month = Calendar.getInstance().get(Calendar.MONTH);
                            int current_date = Calendar.getInstance().get(Calendar.DATE);
                            if (current_month < 6 && current_date < 31) {
                                current_year--;
                            }
                            for(int i=2013;i<=2020;i++){
                                Log.d("yr rain", ""+i);
                                String[] rf = StartActivity.jsontoString((JSONArray) jsonObj.get("Rainfall"+Integer.toString(i)), false);
                                List<String> rf_list = new ArrayList<>();
                                rf_list.addAll(Arrays.asList(rf));
                                for (int j = 0; j < 365 - rf.length; j++) {
                                    rf_list.add("");
                                }
                                String[] rf_of_i = new String[rf_list.size()];
                                rf_list.toArray(rf_of_i);
                                rainfall_db.put(Integer.toString(i), StartActivity.strtoDouble(rf_of_i));
                                rainfall_daily.put(Integer.toString(i), StartActivity.strtoDouble(rf_of_i));
                                if (i >= 2018) {

                                    String[] temp_avg = jsontoString((JSONArray) jsonObj.get("TemperatureAVG"+Integer.toString(i)), false);
                                    String[] temp_min_daily = jsontoString((JSONArray) jsonObj.get("TemperatureMIN"+Integer.toString(i)), false);
                                    String[] temp_max_daily = jsontoString((JSONArray) jsonObj.get("TemperatureMAX"+Integer.toString(i)), false);
                                    String[] temp_min = new String[24*temp_min_daily.length];
                                    String[] temp_max = new String[24*temp_max_daily.length];
                                    for(int t=0;t<temp_min_daily.length;t++){
                                        for(int u=0;u<24;u++){
                                            temp_min[24*t+u] = temp_min_daily[t];
                                            temp_max[24*t+u] = temp_max_daily[t];
                                        }
                                    }
//                                    String[] temp_min = jsontoString((JSONArray) jsonObj.get("temp_daily_min_"+Integer.toString(i)), true);

//                                    String[] temp_max = StartActivity.jsontoString((JSONArray) jsonObj.get("temp_daily_max_"+Integer.toString(i)), true);

                                    String[] temp_hrly_avg_ = StartActivity.jsontoString((JSONArray) jsonObj.get("temp_hourly_avg_"+Integer.toString(i)), true);
                                    String[] rh_hrly_avg_= StartActivity.jsontoString((JSONArray) jsonObj.get("rh_hourly_avg"+Integer.toString(i)),true);
                                    String[] wind_hrly_avg_ = StartActivity.jsontoString((JSONArray) jsonObj.get("wind_hourly_avg_"+Integer.toString(i)),true);
                                    String[] rainfall_hrly_ = StartActivity.jsontoString((JSONArray) jsonObj.get("Rainfallhourly"+Integer.toString(i)),true);
                                    temp_hrly_avg.put(Integer.toString(i), StartActivity.strtoDouble(temp_hrly_avg_));
                                    rh_hrly_avg.put(Integer.toString(i),StartActivity.strtoDouble(rh_hrly_avg_));
                                    wind_hrly_avg.put(Integer.toString(i), StartActivity.strtoDouble(wind_hrly_avg_));
                                    rainfall_hourly.put(Integer.toString(i),  StartActivity.strtoDouble(rainfall_hrly_));

                                    temp_avg_db_daily.put(Integer.toString(i), StartActivity.strtoDouble(temp_avg));
                                    temp_min_db_daily.put(Integer.toString(i), StartActivity.strtoDouble(temp_min_daily));
                                    temp_max_db_daily.put(Integer.toString(i), StartActivity.strtoDouble(temp_max_daily));

                                    temp_max_db_hourly.put(Integer.toString(i), StartActivity.strtoDouble(temp_max));
                                    temp_min_db_hourly.put(Integer.toString(i), StartActivity.strtoDouble(temp_min));
                                    temp_avg_db_hourly.put(Integer.toString(i), StartActivity.strtoDouble(temp_hrly_avg_));

                                }
                            }



                            Toast.makeText(StartActivity.this,circle_db,Toast.LENGTH_LONG).show();
                            setData();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        pDialog.dismiss();

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Log.e("volley error", error.toString());
                        Toast.makeText(StartActivity.this, "Data not found for this location "+error.toString(), Toast.LENGTH_LONG).show();

                    }
                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue  = Volley.newRequestQueue(StartActivity.this);
        requestQueue.add(stringRequest);

        //        try {
//            JSONObject jsonObj = new JSONObject("{\"soil_type\":\"Clayey\",\"soil_depth\":\"Very deep (> 100 cm)\",\"slope\":\"6.21786785125732\",\"District\":\"Amravati\",\"Taluka\":\"Amravati\",\"Circle\":\"Mahuli\",\"lulc_type\":\"Kharif\",\"Rainfall2013\":[\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"39.8\",\"0.0\",\"18.8\",\"9.4\",\"0.0\",\"11.8\",\"0.0\",\"32.8\",\"27.9\",\"21.1\",\"11.4\",\"25.9\",\"0.0\",\"0.0\",\"1.8\",\"0.0\",\"6.9\",\"11.5\",\"9.6\",\"25.4\",\"10.9\",\"0.0\",\"0.0\",\"0.0\",\"24.6\",\"0.0\",\"5.2\",\"15.2\",\"0.0\",\"0.0\",\"1.7\",\"5.3\",\"0.0\",\"0.0\",\"3.3\",\"11.7\",\"18.0\",\"0.0\",\"14.5\",\"24.6\",\"0.0\",\"13.0\",\"0.0\",\"16.2\",\"0.0\",\"0.0\",\"0.0\",\"6.8\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"39.1\",\"81.8\",\"2.8\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"3.4\",\"18.2\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"8.5\",\"0.0\",\"18.0\",\"9.3\",\"26.6\",\"7.9\",\"4.6\",\"1.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"9.7\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"5.4\",\"53.4\",\"0.0\",\"0.0\",\"0.0\",\"10.9\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"50.5\",\"0.0\",\"0.0\",\"0.0\",\"2.3\",\"0.0\",\"6.9\",\"0.0\",\"5.9\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0.0\"],\"Rainfall2014\":[\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"7.6\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"83.4\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.3\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"27.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.8\",\"0.0\",\"0.0\",\"4.0\",\"56.4\",\"0.0\",\"7.4\",\"0.0\",\"24.0\",\"7.2\",\"7.2\",\"204.0\",\"38.4\",\"0.0\",\"0.0\",\"0.0\",\"36.0\",\"3.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"26.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"46.0\",\"0.0\",\"9.6\",\"6.4\",\"60.0\",\"21.0\",\"61.8\",\"17.0\",\"5.4\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"50.0\",\"39.4\",\"19.4\",\"0.0\",\"0.0\",\"3.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.1\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"2.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0.0\",\"0\"],\"Rainfall2015\":[\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"3.5\",\"0.0\",\"0.0\",\"0.0\",\"11.0\",\"2.0\",\"34.6\",\"0.0\",\"16.0\",\"6.8\",\"0.0\",\"26.4\",\"14.5\",\"62.2\",\"13.4\",\"17.0\",\"3.4\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.4\",\"8.3\",\"0.0\",\"28.0\",\"42.6\",\"12.0\",\"0.0\",\"0.0\",\"0.0\",\"14.6\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"36.0\",\"156.0\",\"13.0\",\"0.0\",\"4.0\",\"0.0\",\"28.0\",\"0.0\",\"59.2\",\"4.0\",\"24.4\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.0\",\"17.9\",\"7.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.0\",\"0.0\",\"0.0\",\"5.8\",\"58.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"4.7\",\"0.0\",\"0.0\",\"0.0\",\"8.3\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"Rainfall2016\":[\"0.0\",\"0.0\",\"2.4\",\"0.0\",\"0.0\",\"0.0\",\"28.3\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"11.6\",\"2.6\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"24.8\",\"0.0\",\"0.0\",\"10.6\",\"53.4\",\"61.8\",\"15.6\",\"28.5\",\"46.4\",\"46.4\",\"4.3\",\"11.7\",\"3.0\",\"0.0\",\"0.0\",\"69.0\",\"17.2\",\"31.8\",\"12.8\",\"12.1\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"6.0\",\"3.0\",\"2.2\",\"0.0\",\"1.4\",\"13.1\",\"68.5\",\"0.0\",\"11.8\",\"19.4\",\"1.7\",\"0.0\",\"0.0\",\"0.0\",\"22.0\",\"2.9\",\"13.0\",\"7.0\",\"0.0\",\"53.6\",\"0.0\",\"0.0\",\"5.3\",\"4.3\",\"0.0\",\"0.0\",\"0.0\",\"15.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"2.1\",\"1.4\",\"14.5\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"10.0\",\"8.0\",\"5.3\",\"0.0\",\"0.0\",\"0.0\",\"5.3\",\"12.3\",\"0.0\",\"2.0\",\"0.0\",\"10.3\",\"7.3\",\"0.0\",\"0.0\",\"0.0\",\"13.6\",\"22.4\",\"0.5\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"46.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0.0\",\"0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0.0\",\"0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\"],\"Rainfall2017\":[\"0.0\",\"2.2\",\"0.0\",\"0.0\",\"0.0\",\"10.9\",\"8.9\",\"0.0\",\"0.0\",\"0.0\",\"25.2\",\"1.4\",\"0.0\",\"3.3\",\"0.0\",\"17.3\",\"19.2\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.2\",\"0.0\",\"0.0\",\"11.4\",\"0.0\",\"47.2\",\"2.1\",\"0.0\",\"6.4\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"5.1\",\"9.6\",\"6.0\",\"2.0\",\"0.0\",\"0.0\",\"0.0\",\"10.3\",\"16.0\",\"4.1\",\"4.0\",\"1.4\",\"10.3\",\"11.7\",\"23.6\",\"8.6\",\"38.4\",\"8.0\",\"5.1\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"4.3\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"16.0\",\"22.4\",\"14.6\",\"0.0\",\"4.6\",\"0.0\",\"1.2\",\"0.0\",\"0.0\",\"0.0\",\"12.5\",\"29.6\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"9.1\",\"0.0\",\"5.3\",\"17.4\",\"6.0\",\"4.1\",\"11.1\",\"3.0\",\"9.4\",\"0.0\",\"0.0\",\"5.1\",\"8.3\",\"35.8\",\"9.2\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"25.6\",\"0.0\",\"0.0\",\"12.4\",\"18.3\",\"0.0\",\"0.0\",\"0.0\",\"21.6\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0.0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]}");
//            if (jsonObj.get("soil_depth").equals(null))
//                soil_depth_db = "NULL";
//            else
//                soil_depth_db = (String) jsonObj.get("soil_depth");
//            if (jsonObj.get("soil_type").equals(null))
//                soil_type_db = "NULL";
//            else
//                soil_type_db = (String) jsonObj.get("soil_type");
//            if (jsonObj.get("lulc_type").equals(null))
//                lulc_type_db = "NULL";
//            else
//                lulc_type_db = (String) jsonObj.get("lulc_type");
//            if (jsonObj.get("slope").equals(null))
//                slope_db = 1.0;
//            else
//                slope_db = new Double((String) jsonObj.get("slope"));
//            Log.d("soil_dep", soil_depth_db);
//            Log.d("soil_type", soil_type_db);
//            Log.d("lulc_t", lulc_type_db);
//            Log.d("slope", slope_db.toString());
//            district_db = (String) jsonObj.get("District");
//            String[] rf2013 = jsontoString((JSONArray) jsonObj.get("Rainfall2013"));
//            String[] rf2014 = jsontoString((JSONArray) jsonObj.get("Rainfall2014"));
//            String[] rf2015 = jsontoString((JSONArray) jsonObj.get("Rainfall2015"));
//            String[] rf2016 = jsontoString((JSONArray) jsonObj.get("Rainfall2016"));
//            String[] rf2017 = jsontoString((JSONArray) jsonObj.get("Rainfall2017"));
//            rainfall_db = new HashMap<>();
//            rainfall_db.put("2013", strtoDouble(rf2013));
//            rainfall_db.put("2014", strtoDouble(rf2014));
//            rainfall_db.put("2015", strtoDouble(rf2015));
//            rainfall_db.put("2016", strtoDouble(rf2016));
//            rainfall_db.put("2017", strtoDouble(rf2017));
//        } catch (JSONException e){
//            e.printStackTrace();
//        }

//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestFuture<JSONObject> requestFuture=RequestFuture.newFuture();
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,null,requestFuture,requestFuture);
//        MySingleton.getInstance(StartActivity.this).addToRequestQueue(jsonObjectRequest);
//            try {
//
////                Log.d("JSON",response.toString());
//                JSONObject jsonObj = requestFuture.get(10,TimeUnit.SECONDS);
//                if(jsonObj.get("soil_depth").equals(null))
//                    soil_depth_db = "NULL";
//                else
//                    soil_depth_db = (String) jsonObj.get("soil_depth");
//                if(jsonObj.get("soil_type").equals(null))
//                    soil_type_db = "NULL";
//                else
//                    soil_type_db = (String) jsonObj.get("soil_type");
//                if(jsonObj.get("lulc_type").equals(null))
//                    lulc_type_db = "NULL";
//                else
//                    lulc_type_db = (String) jsonObj.get("lulc_type");
//                if(jsonObj.get("slope").equals(null))
//                    slope_db = 1.0 ;
//                else
//                    slope_db = new Double((String) jsonObj.get("slope"));
//                Log.d("soil_dep",soil_depth_db);
//                Log.d("soil_type",soil_type_db);
//                Log.d("lulc_t",lulc_type_db);
//                Log.d("slope",slope_db.toString());
//                district_db = (String) jsonObj.get("District");
//                String[] rf2013 =jsontoString((JSONArray)  jsonObj.get("Rainfall2013"));
//                String[] rf2014 =jsontoString((JSONArray)  jsonObj.get("Rainfall2014"));
//                String[] rf2015 =jsontoString((JSONArray)  jsonObj.get("Rainfall2015"));
//                String[] rf2016 =jsontoString((JSONArray)  jsonObj.get("Rainfall2016"));
//                String[] rf2017 =jsontoString((JSONArray)  jsonObj.get("Rainfall2017"));
//                rainfall_db = new HashMap<> ();
//                rainfall_db.put("2013", strtoDouble(rf2013));
//                rainfall_db.put("2014", strtoDouble(rf2014));
//                rainfall_db.put("2015", strtoDouble(rf2015));
//                rainfall_db.put("2016", strtoDouble(rf2016));
//                rainfall_db.put("2017", strtoDouble(rf2017));
//
//
//            } catch (Exception e) {
//                Log.d(" code","htiya");
//                e.printStackTrace();
//                }
//            pDialog.dismiss();
//



    }


    static Double [] strtoDouble(String [] arr ) {
        Double [] arrDouble = new Double[arr.length];
        for (int i=0;i<arr.length;i++){
            if (arr[i].equals("None") || arr[i].trim().equals("")) {
                arrDouble[i] = 0.0;
            } else if (arr[i].equals("-9999")) {
                arrDouble[i] = Double.valueOf(30.0d);
            } else {
                arrDouble[i] = new Double(arr[i]);
            }
        }

        return arrDouble;
    }

    static String [] jsontoString(JSONArray jsArray, boolean hourly) throws JSONException {
        String[] array;
        if(!hourly)
            array= new String[366];
        else
            array = new String[24*366];
        Log.d("arrlen", ""+jsArray.length());
        for (int i = 0; i < jsArray.length(); i++)
            array[i] = (String) jsArray.get(i);

        //for partial data
        if(!hourly){
                for(int i=jsArray.length();i<366;i++){
                    array[i] = "";
            }
        }else{
            for(int i=jsArray.length();i<24*366;i++) {
                array[i] = "";
            }
        }

        return array;

    }

    static double [] dlbTodouble(Double [] arr ) {
        double [] arrDouble = new double[arr.length];
        for (int i=0;i<arr.length;i++)
            arrDouble[i]=(double) (arr[i]);
        return arrDouble;
    }

    public static long getDifferenceDays(String d1, String year_sel) {
        String d2 = "01/06/"+ year_sel;

        long diff = 0;
        try {
            diff = ( new SimpleDateFormat("dd/MM/yyyy").parse(d1).getTime() - new SimpleDateFormat("dd/MM/yyyy").parse(d2).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}

