package com.example.sudhanshu.gis;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by swapnil on 7/9/18.
 */

public class MyExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
    private final Context myContext;
    private final Class<?> myActivityClass;
    public MyExceptionHandler(Context context, Class<?> c) {
        myContext = context;
        myActivityClass = c;
    }
    public void uncaughtException(Thread thread, Throwable exception) {
        // To know what causes the exception and in which activity
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);
        Log.d("Error", String.valueOf(stackTrace));// You can use LogCat too
        Intent intent = new Intent(this.myContext, MapActivity.class);

        String s = stackTrace.toString();
        intent.putExtra("uncaughtException", "Exception is: " + stackTrace.toString());
        intent.putExtra("stacktrace", s);
        // To clear the previous tasks from stack
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        myContext.startActivity(intent);

        // myPid returns the identifier of this process for restarting the Activity
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }
}