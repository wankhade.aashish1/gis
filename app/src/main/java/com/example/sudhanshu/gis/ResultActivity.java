package com.example.sudhanshu.gis;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.StringUtils;

import org.w3c.dom.Text;

import java.io.FileOutputStream;
import java.io.IOException;

public class ResultActivity extends AppCompatActivity {
    LineChart lineChart,lineChart1;
    CombinedChart combinedChart,combinedChart1;
    String Reportname;




    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Button savebtn=(Button) findViewById(R.id.savebtn);
        final kharifModel km = (kharifModel)getIntent().getSerializableExtra("serialize data");
        boolean isChecked = this.getIntent().getExtras().getBoolean("checkBoxValue", false);
        final String farmer_name=this.getIntent().getExtras().getString("farmername");
        final String circle_name=this.getIntent().getExtras().getString("circlename");
        final String year_selected=this.getIntent().getExtras().getString("year");
        final String irrigation_amount=this.getIntent().getExtras().getString("irrigation_amount");
        final String per_irrigation_water=this.getIntent().getExtras().getString("per_irrigation_water");
        final String irrigation_dates=this.getIntent().getExtras().getString("irrigation_dates");




        int sow_offset=this.getIntent().getIntExtra("sowing_offset",0);
//        Toast.makeText(this, ""+sow_offset, Toast.LENGTH_SHORT).show();





        Reportname=org.apache.commons.lang3.StringUtils.capitalize(farmer_name)+"_"+km.district+"_"+km.crop+".pdf";

//        Toast.makeText(this, ""+isChecked, Toast.LENGTH_SHORT).show();



        final Date date = new Date();

        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyStoragePermissions(ResultActivity.this);
                CombinedChart saveChart = (CombinedChart) findViewById(R.id.chart);
                saveChart.saveToGallery("chart",50);

                CombinedChart saveChart1 = (CombinedChart) findViewById(R.id.chart1);
                saveChart1.saveToGallery("chart1",50);

//
//                Document doc = new Document();
//                try {
//                    PdfWriter.getInstance(doc, new FileOutputStream("ImageDemo.pdf"));
//                    doc.open();
//
//                    // Creating image by file name
//                    String filename = "chart.jpg";
//                    Image image = Image.getInstance(filename);
//                    doc.add(image);
//
//                    // The following line to prevent the "Server returned
//                    // HTTP response code: 403" error.
//                    System.setProperty("http.agent", "Chrome");
//
//                    // Creating image from a URL
//                    String url = "chart1.jpg";
//                    image = Image.getInstance(url);
//                    doc.add(image);
//                    doc.close();
//                } catch (DocumentException | IOException e) {
//                    e.printStackTrace();
//                } finally {
////                    doc.close();
//                }


                Document document = new Document();

                try {
//                     Reportname=org.apache.commons.lang3.StringUtils.capitalize(farmer_name)+"_"+km.district+"_"+km.crop+".pdf";
                    File f = new File(Environment.getExternalStorageDirectory(), Reportname);
                    PdfWriter.getInstance(document, new FileOutputStream(f));
                    document.open();
                    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);


                    document.add(new Paragraph("                                       Farm Based Water Balance",boldFont));
                    document.add(new Paragraph("                              Project:-PoCRA,Government of Maharashtra,IIT Bombay-2019"));
                    document.add(new Paragraph("                         For any queries or comments please contact us at pocra@cse.iitb.ac.in"));


                    document.add(new Paragraph("  "));
                            document.add(new Paragraph(String.valueOf(date)));
                    document.add(new Paragraph("--------------------------------------------------------"));
                    document.add(new Paragraph("Farmer : " +farmer_name));
                    document.add(new Paragraph("Year : " +year_selected));
                    document.add(new Paragraph("District : " +km.district));
                    document.add(new Paragraph("Circle : " +circle_name));
                    document.add(new Paragraph("Lattitude : " +km.lat));
                    document.add(new Paragraph("Longitude : " +km.logni));

                    document.add(new Paragraph("Crop : " +km.crop));
                    document.add(new Paragraph("Soil Type : " +km.soil_type));
                    document.add(new Paragraph("Depth Value :" +String.valueOf(km.depth_value)));
                    document.add(new Paragraph("Per_irrigation_water:  "+ per_irrigation_water));
                    document.add(new Paragraph("Irrigation_amount:  "+ irrigation_amount));
                    document.add(new Paragraph("Irrigation_dates:  "+ irrigation_dates));
                    document.add(new Paragraph("  "));
                    document.add(new Paragraph("  "));

                    document.add(new Paragraph("--------------------------------------------------------"));

//                    document.add(new Paragraph("Monsoon End Values:-",boldFont));
//
//                    document.add(new Paragraph("--------------------------------------------------------"));
//
//                    TextView Raintxt = (TextView) findViewById(R.id.rainfall);
//                    document.add(new Paragraph((String) Raintxt.getText()));
////                    Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
//                    TextView runofftxt = (TextView) findViewById(R.id.runoff);
//                    document.add(new Paragraph((String) runofftxt.getText()));
////                    runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");
//
//                    TextView aettxt = (TextView) findViewById(R.id.aet);
//                    document.add(new Paragraph((String) aettxt.getText()));
////                    aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
//                    TextView smtxt = (TextView) findViewById(R.id.soil_moist);
//                    document.add(new Paragraph((String) smtxt.getText()));
////                    smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
//                    TextView gwrtxt = (TextView) findViewById(R.id.gwr);
//                    document.add(new Paragraph((String) gwrtxt.getText()));
////                    gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
//                    TextView defecittxt = (TextView) findViewById(R.id.defecit);
//                    document.add(new Paragraph((String) defecittxt.getText()));
////                    defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");
//                    TextView dry_spells = (TextView) findViewById(R.id.dry_spells);
//                    document.add(new Paragraph((String) dry_spells.getText()));
//                    document.add(new Paragraph("  "));
//                    document.add(new Paragraph("  "));
//
//
//                    document.add(new Paragraph("#######################################################"));
//                    document.add(new Paragraph("  "));
//                    document.add(new Paragraph("  "));
//
//
//                    document.add(new Paragraph("Crop End Values:-",boldFont));
//
//                    document.add(new Paragraph("--------------------------------------------------------"));
//
//                    TextView Raintxt_CE = (TextView) findViewById(R.id.rainfall_ce);
//                    document.add(new Paragraph((String) Raintxt_CE.getText()));
//
////                    Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
//                    TextView runofftxt_CE = (TextView) findViewById(R.id.runoff_ce);
//                    document.add(new Paragraph((String) runofftxt_CE.getText()));
////                    runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
//                    TextView aettxt_CE = (TextView) findViewById(R.id.aet_ce);
//                    document.add(new Paragraph((String) aettxt_CE.getText()));
////                    aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
//                    TextView smtxt_CE = (TextView) findViewById(R.id.soil_moist_ce);
//                    document.add(new Paragraph((String) smtxt_CE.getText()));
////                    smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
//                    TextView gwrtxt_CE = (TextView) findViewById(R.id.gwr_ce);
//                    document.add(new Paragraph((String) gwrtxt_CE.getText()));
////                    gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
//                    TextView defecittxt_CE = (TextView) findViewById(R.id.defecit_ce);
//                    document.add(new Paragraph((String) defecittxt_CE.getText()));
////                    defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");


                    document.add(new Paragraph("Monsoon End Values:-",boldFont));


                    document.add(new Paragraph("--------------------------------------------------------"));

                    TextView Raintxt = (TextView) findViewById(R.id.textView5);
                    document.add(new Paragraph("Rainfall in Monsoon: "+(String) Raintxt.getText()));
//                    Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
                    TextView runofftxt = (TextView) findViewById(R.id.textView8);
                    document.add(new Paragraph("Runoff in Monsoon: "+ (String) runofftxt.getText()));
//                    runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");

                    TextView aettxt = (TextView) findViewById(R.id.textView11);
                    document.add(new Paragraph("Total crop AET in Monsoon: "+(String) aettxt.getText()));
//                    aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
                    TextView smtxt = (TextView) findViewById(R.id.textView14);
                    document.add(new Paragraph("Soil Moisture at Monsoon End: "+(String) smtxt.getText()));
//                    smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
                    TextView gwrtxt = (TextView) findViewById(R.id.textView17);
                    document.add(new Paragraph("GW Recarge in Monsoon: "+ (String) gwrtxt.getText()));
//                    gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
                    TextView defecittxt = (TextView) findViewById(R.id.textView20);
                    document.add(new Paragraph("Total Deficit in Monsoon: "+ (String) defecittxt.getText()));
//                    defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");

                    document.add(new Paragraph("  "));


                    document.add(new Paragraph("#######################################################"));
                    document.add(new Paragraph("  "));
                    document.add(new Paragraph("  "));


                    document.add(new Paragraph("Crop End Values:-",boldFont));

                    document.add(new Paragraph("--------------------------------------------------------"));

                    TextView Raintxt_CE = (TextView) findViewById(R.id.textView6);
                    document.add(new Paragraph("Rainfall in CE: "+(String) Raintxt_CE.getText()));

//                    Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
                    TextView runofftxt_CE = (TextView) findViewById(R.id.textView9);
                    document.add(new Paragraph("Runoff in CE: "+(String) runofftxt_CE.getText()));
//                    runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
                    TextView aettxt_CE = (TextView) findViewById(R.id.textView12);
                    document.add(new Paragraph("Total crop AET in CE: "+(String) aettxt_CE.getText()));
//                    aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
                    TextView smtxt_CE = (TextView) findViewById(R.id.textView15);
                    document.add(new Paragraph("Soil Moisture at CE: "+ (String) smtxt_CE.getText()));
//                    smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
                    TextView gwrtxt_CE = (TextView) findViewById(R.id.textView18);
                    document.add(new Paragraph("GW Recarge in CE: "+ (String) gwrtxt_CE.getText()));
//                    gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
                    TextView defecittxt_CE = (TextView) findViewById(R.id.textView21);
                    document.add(new Paragraph("Total Deficit in CE: "+(String) defecittxt_CE.getText()));
//                    defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");

                    document.newPage();



                    TextView wb = (TextView) findViewById(R.id.wb);
                    document.add(new Paragraph((String) wb.getText(),boldFont));

                    document.add(new Paragraph("  "));


                    TextView daily = (TextView) findViewById(R.id.daily);
                    document.add(new Paragraph(String.valueOf(daily.getText())));

                    Image image = Image.getInstance("/storage/emulated/0/DCIM/chart.jpg");
                    image.scalePercent(50f);
                    document.add(image);

                    document.newPage();

                    TextView cumulative = (TextView) findViewById(R.id.cumulative);
                    document.add(new Paragraph(String.valueOf(cumulative.getText())));

                    image = Image.getInstance("/storage/emulated/0/DCIM/chart1.jpg");
                    image.scalePercent(50f);
                    document.add(image);

                    document.newPage();






//                    wb.setText(wb.getText()+km.crop+" at ("+km.lat.substring(0,Math.min(km.lat.length(),5)) +", "+km.logni.substring(0,Math.min(km.logni.length(),5))+")");


//                    daily.setText(daily.getText()+" for crop "+ km.crop);
//                    cumulative.setText(cumulative.getText()+" for crop "+ km.crop);


//                    appendLog(date.toString());

                    document.close();

                    Toast.makeText(ResultActivity.this, "Report Generated", Toast.LENGTH_SHORT).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }

        });
//        View root = findViewById(R.id.your_root);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//        Log.d("Result Start", "Result start ");






        combinedChart = (CombinedChart) findViewById(R.id.chart);
        int crop_end_index = km.crop_end_index -1;
        String[] xaxes = new String[crop_end_index+1];
        ArrayList<Entry> aet = new ArrayList<>();
        ArrayList<Entry> pet = new ArrayList<>();
        ArrayList<Entry> sm = new ArrayList<>();
        ArrayList<Entry> runoff = new ArrayList<>();
        ArrayList<BarEntry> rainfall = new ArrayList<>();
        ArrayList<BarEntry> irrigation = new ArrayList<>();
        ArrayList<Entry> vulnerability = new ArrayList<>();

        ArrayList<Entry> ground_water_recharge = new ArrayList<>();


        ArrayList<Entry> rain_s = new ArrayList<>();
        ArrayList<Entry> runoff_s = new ArrayList<>();
        ArrayList<Entry> gwr_s = new ArrayList<>();

        List<Double> pet_val = km.pet;
        List <Double> aet_val =km.aet;

        double vuln_sum=0;
        double rain_sum=0;
        double runoff_sum=0;
        double gwr_sum=0;
//        double [] vulnerability = new double[crop_end_index+1];

//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    while (progressDialog.getProgress() <= progressDialog
//                            .getMax()) {
//                        Thread.sleep(200);
//                        handle.sendMessage(handle.obtainMessage());
//                        if (progressDialog.getProgress() == progressDialog
//                                .getMax()) {
//                            progressDialog.dismiss();
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();


        if(isChecked)
        {


            appendLog(date.toString());
            appendLog("--------------------------------------------------------------------------");
            appendLog("Year :" +String.valueOf(km.year_sel));
            appendLog("District : " +km.district);
            appendLog("Circle :" +String.valueOf(circle_name));
            appendLog("Lattitude : " +km.lat);
            appendLog("Longitude : " +km.logni);
            appendLog("Crop : " +km.crop);
            appendLog("Soil Type : " +km.soil_type);
            appendLog("Depth_Value :" +String.valueOf(km.depth_value));
            appendLog("--------------------------------------------------------------------------");
            for (int i =0;i<=crop_end_index;i++){
                aet.add(new Entry(i,km.budget.aet[i]));

                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));

                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                xaxes[i]=Integer.toString(i);
//                Log.d("Day "+i+" AET ", Double.toString(aet_val.get(i)));
//                Log.d("Day "+i+" PET ", Double.toString(pet_val.get(i)));
//                Log.d("Day "+i+" SM ", Double.toString(km.budget.sm[i]));
//                Log.d("Day "+i+" Runoff ", Double.toString(km.budget.runoff[i]));
//                Log.d("Day "+i+" Rainfall ", Double.toString((float)km.rainfall[i]));
//                Log.d("Day "+i+" Irrigation ", Double.toString((float)km.irrgation[i]));
//                Log.d("Day "+i+" Rainfall+Irrigation ", Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]));
//                Log.d("Day "+i+" Ground Water Recharge ", Double.toString((float)km.budget.GW_rech[i]));

                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
                appendLog(aet_f);
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
                appendLog(pet_f);
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
                appendLog(SM_f);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
                appendLog(Runoff_f);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
                appendLog(Rainfall_f);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i]);
                appendLog(Irrigation_f);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]);
                appendLog(RainIrri_f);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);
                appendLog(RainIrri_gwr);



                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];

                gwr_sum += km.budget.GW_rech[i];


                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));

            }

            appendLog("---------------------------------------------------------------------------------------------------------");

            Toast.makeText(this, "Daily Values added to text file", Toast.LENGTH_SHORT).show();
        }
        else
        {
//            appendLog(date.toString());
//            appendLog("--------------------------------------------------------------------------");
//            appendLog("District : " +km.district);
//            appendLog("Lattitude : " +km.lat);
//            appendLog("Longitude : " +km.logni);
//            appendLog("Crop : " +km.crop);
//            appendLog("Soil Type : " +km.soil_type);
//            appendLog("Depth_Value :" +String.valueOf(km.depth_value));
//
//            appendLog("--------------------------------------------------------------------------");
            for (int i =0;i<=crop_end_index;i++){
                aet.add(new Entry(i,km.budget.aet[i]));

                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));

                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                xaxes[i]=Integer.toString(i);
//                Log.d("Day "+i+" AET ", Double.toString(aet_val.get(i)));
//                Log.d("Day "+i+" PET ", Double.toString(pet_val.get(i)));
//                Log.d("Day "+i+" SM ", Double.toString(km.budget.sm[i]));
//                Log.d("Day "+i+" Runoff ", Double.toString(km.budget.runoff[i]));
//                Log.d("Day "+i+" Rainfall ", Double.toString((float)km.rainfall[i]));
//                Log.d("Day "+i+" Irrigation ", Double.toString((float)km.irrgation[i]));
//                Log.d("Day "+i+" Rainfall+Irrigation ", Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]));
//                Log.d("Day "+i+" Ground Water Recharge ", Double.toString((float)km.budget.GW_rech[i]));

                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
//                appendLog(aet_f);
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
//                appendLog(pet_f);
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
//                appendLog(SM_f);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
//                appendLog(Runoff_f);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
//                appendLog(Rainfall_f);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i]);
//                appendLog(Irrigation_f);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]);
//                appendLog(RainIrri_f);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);
//                appendLog(RainIrri_gwr);



                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];

                gwr_sum += km.budget.GW_rech[i];


                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));

            }

//            appendLog("---------------------------------------------------------------------------------------------------------");


        }



//        IAxisValueFormatter
        //progressDialog.dismiss();
        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        LineDataSet lineDataSet1 = new LineDataSet(aet,"AET");
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setColor(Color.GREEN);
//        LineData ld1 = new LineData();
//        ld1.addDataSet(lineDataSet1);

        LineDataSet lineDataSet2 = new LineDataSet(pet,"PET");
        lineDataSet2.setDrawCircles(false);
        lineDataSet2.setColor(Color.RED);
//        LineData ld2 = new LineData();
//        ld2.addDataSet(lineDataSet2);


//        LineDataSet lineDataSet3 = new LineDataSet(sm,"Soil Moisture");
//        lineDataSet3.setDrawCircles(false);
//        lineDataSet3.setColor(Color.MAGENTA);

//        LineDataSet lineDataSet4 = new LineDataSet(runoff,"Runoff");
//        lineDataSet4.setDrawCircles(false);
//        lineDataSet4.setColor(Color.CYAN);

        BarDataSet barDataSet5 = new BarDataSet(rainfall,"Rainfall");
//        barDataSet5.setDrawCircles(false);
        barDataSet5.setColor(Color.BLUE);
        BarDataSet barDataSet6 = new BarDataSet(irrigation,"Irrigation");
//        barDataSet5.setDrawCircles(false);
        barDataSet6.setColor(Color.RED);


        BarData bd5 =new BarData();
        bd5.addDataSet(barDataSet5);
        bd5.addDataSet(barDataSet6);



        lineDataSets.add(lineDataSet1);
        lineDataSets.add(lineDataSet2);
//        lineDataSets.add(lineDataSet3);
//        lineDataSets.add(lineDataSet4);
        CombinedData combinedData = new CombinedData();
        combinedData.setData(new LineData(lineDataSets));
        combinedData.setData(bd5);

        combinedChart.setData(combinedData);
        Description des1 = combinedChart.getDescription();
        des1.setText("Daily values                                                                          ");
//        des1.setEnabled(false);

        combinedChart1 = (CombinedChart) findViewById(R.id.chart1);
        ArrayList<ILineDataSet> lineDataSets1 = new ArrayList<>();

        LineDataSet lineDataSet6 = new LineDataSet(vulnerability,"PET- AET");
        lineDataSet6.setDrawCircles(false);
        lineDataSet6.setColor(Color.RED);

//        LineDataSet lineDataSet7 = new LineDataSet(rain_s,"Rainfall");
//        lineDataSet7.setDrawCircles(false);
//        lineDataSet7.setColor(Color.BLUE);

        LineDataSet lineDataSet8 = new LineDataSet(runoff_s,"Runoff");
        lineDataSet8.setDrawCircles(false);
        lineDataSet8.setColor(Color.MAGENTA);


        LineDataSet lineDataSet9 = new LineDataSet(gwr_s,"GW");
        lineDataSet9.setDrawCircles(false);
        lineDataSet9.setColor(Color.GREEN);


        lineDataSets1.add(lineDataSet6);
//        lineDataSets1.add(lineDataSet7);
        lineDataSets1.add(lineDataSet8);
//        lineChart1.setData(new LineData(lineDataSets1));

        lineDataSets1.add(lineDataSet9);


        CombinedData combinedData1 = new CombinedData();
        combinedData1.setData(new LineData(lineDataSets1));
        combinedData1.setData(bd5);

        combinedChart1.setData(combinedData1);
        Description des = combinedChart1.getDescription();
        des.setText("Daily values                                                                         ");
//        des1.setEnabled(false);

//        verifyStoragePermissions(this);
//        CombinedChart saveChart = (CombinedChart) findViewById(R.id.chart);
//        saveChart.saveToGallery("chart",50);



//        double gwr_sum_monsoon=0,rain_sum_monsoon=0,runoff_sum_monsoon=0,defecit_sum_monsoon=0;
//        for (int i=0;i<=km.monsoon_end_index;i++){
//            rain_sum_monsoon+= km.rainfall[i];
//            runoff_sum_monsoon+= km.runoff.get(i);
//            defecit_sum_monsoon+= km.pet.get(i) - km.aet.get(i);
//            gwr_sum_monsoon += km.GW_rech.get(i);
//        }



        TextView wb = (TextView) findViewById(R.id.wb);
//        wb.setText(wb.getText()+km.crop+" at ("+km.lat +", "+km.logni+")");
        //wb.setText(wb.getText()+km.crop+" at ("+km.lat.substring(0,Math.min(km.lat.length(),5)) +", "+km.logni.substring(0,Math.min(km.logni.length(),5))+")");
        wb.setText(wb.getText()+"पीक: "+km.crop+" स्थान: "+km.lat.substring(0,Math.min(km.lat.length(),5)) +", "+km.logni.substring(0,Math.min(km.logni.length(),5)));
//        Log.d("Location values :" ,km.logni);
        TextView daily = (TextView) findViewById(R.id.daily);
        TextView cumulative = (TextView) findViewById(R.id.cumulative);
        //daily.setText(daily.getText()+" for crop "+ km.crop);
        daily.setText(km.crop+" पिकासाठी  "+daily.getText());
        cumulative.setText(km.crop+" पिकासाठी  "+cumulative.getText());

        String dtt = km.year_sel+"-06-01";// Start date
//        Toast.makeText(this, ""+dtt, Toast.LENGTH_SHORT).show();
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cc = Calendar.getInstance();
        try {
            cc.setTime(sdff.parse(dtt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cc.add(Calendar.DATE, sow_offset);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        String output = sdf1.format(cc.getTime());





        TextView Sowing_datetxt= ( TextView )findViewById(R.id.Sowing_date);
        Sowing_datetxt.setText("पेरणीची तारीखः "+ output);
        TextView Starting_Soil_Moisture= ( TextView )findViewById(R.id.Starting_Soil_Moisture);
//        Starting_Soil_Moisture.setText("Starting Soil Moisture: "+ (Math.round(km.starting_soil_moisture * 1000))+ " mm");


        //Original Values

//        TextView Raintxt = (TextView) findViewById(R.id.rainfall);
//        Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
//        TextView runofftxt = (TextView) findViewById(R.id.runoff);
//        runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");
//        TextView aettxt = (TextView) findViewById(R.id.aet);
//        aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
//        TextView smtxt = (TextView) findViewById(R.id.soil_moist);
//        smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
//        TextView gwrtxt = (TextView) findViewById(R.id.gwr);
//        gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
//        TextView defecittxt = (TextView) findViewById(R.id.defecit);
//        defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");



        TextView dry_spells = (TextView) findViewById(R.id.dry_spells);
        Calendar c = Calendar.getInstance();
        String dry_spell_report = new String ();
        int count =0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for(int key:km.budget.dry_spells.keySet()){
            c.set(Integer.parseInt(km.year_sel),05,01);
            c.add(Calendar.DATE,key);
            String start_date = sdf.format(c.getTime());
            c.add(Calendar.DATE,km.budget.dry_spells.get(key));
            String end_date = sdf.format(c.getTime());
            count+=1;
            dry_spell_report += "पाण्याचा खंड "+count+":" + start_date +" पासून "+ end_date+" पर्यंत"+"\n";
//            Log.d("Dry spell",Integer.toString(key));
        }
        dry_spells.setText(dry_spell_report);


        // Original Values

//        TextView Raintxt_CE = (TextView) findViewById(R.id.rainfall_ce);
//        Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
//        TextView runofftxt_CE = (TextView) findViewById(R.id.runoff_ce);
//        runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
//        TextView aettxt_CE = (TextView) findViewById(R.id.aet_ce);
//        aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
//        TextView smtxt_CE = (TextView) findViewById(R.id.soil_moist_ce);
//        smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
//        TextView gwrtxt_CE = (TextView) findViewById(R.id.gwr_ce);
//        gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
//        TextView defecittxt_CE = (TextView) findViewById(R.id.defecit_ce);
//        defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");

        int selected = getIntent().getIntExtra("selected",1);



        TextView row1 = (TextView) findViewById(R.id.textView1);
        TextView row2 = (TextView) findViewById(R.id.textView2);
        TextView row3 = (TextView) findViewById(R.id.textView3);
        TextView row4 = (TextView) findViewById(R.id.textView4);
        TextView row5 = (TextView) findViewById(R.id.textView5);
        TextView row6 = (TextView) findViewById(R.id.textView6);
        TextView row7 = (TextView) findViewById(R.id.textView7);
        TextView row8 = (TextView) findViewById(R.id.textView8);
        TextView row9 = (TextView) findViewById(R.id.textView9);
        TextView row10 = (TextView) findViewById(R.id.textView10);
        TextView row11 = (TextView) findViewById(R.id.textView11);
        TextView row12 = (TextView) findViewById(R.id.textView12);

        TextView row13 = (TextView) findViewById(R.id.textView13);
        TextView row14 = (TextView) findViewById(R.id.textView14);
        TextView row15 = (TextView) findViewById(R.id.textView15);
        TextView row16 = (TextView) findViewById(R.id.textView16);
        TextView row17 = (TextView) findViewById(R.id.textView17);
        TextView row18 = (TextView) findViewById(R.id.textView18);
        TextView row19 = (TextView) findViewById(R.id.textView19);
        TextView row20 = (TextView) findViewById(R.id.textView20);
        TextView row21 = (TextView) findViewById(R.id.textView21);






        row1.setText("परिमापक");
        row2.setText("मान्सून समाप्ती ");
        row3.setText("पीक समाप्ती ");
        row4.setText("पाऊस");
        row5.setText((Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
        row6.setText((Math.round(km.budget.rain_sum_crop_end))+ " mm");
        row7.setText("अपधाव (Runoff) ");
        row8.setText((Math.round(km.budget.runoff_monsoon_end))+ " mm");
        row9.setText((Math.round(km.budget.runoff_crop_end))+ " mm");
       // row10.setText("एकूण पीक एईटी");
        row10.setText("पिकाने घेतलेले पाणी (AET)");
        row11.setText((Math.round(km.budget.aet_monsoon_end))+ " mm");
        row12.setText((Math.round(km.budget.aet_crop_end))+ " mm");



        row13.setText("मातीचा ओलावा");
        row14.setText(Math.round(km.budget.sm_monsoon_end)+ " mm" );
        row15.setText(Math.round(km.budget.sm_crop_end)+ " mm");
        row16.setText("भूजल भरणा/रिचार्ज");
        row17.setText((Math.round(km.budget.gwr_monsoon_end)+ " mm"));
        row18.setText((Math.round(km.budget.gwr_crop_end)+ " mm"));
        row19.setText("एकूण तूट");
        row20.setText((Math.round(km.budget.defecit_sum_monsoon))+ " mm");
        row21.setText((Math.round(km.budget.defecit_sum_crop))+ " mm");






        Log.d("Result Finish", "Result fin ");


//        combinedChart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(ResultActivity.this, "Its Working", Toast.LENGTH_SHORT).show();
//            }
//        });

    }



    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    public void saveChart(View view) {

        BarChart saveChart = (BarChart) findViewById(R.id.chart);
        saveChart.saveToGallery("chart",50);

    }

    public void appendLogT(String text){
    }

    public void appendLog(String text)
    {

//        final ProgressDialog progressDialog = new ProgressDialog(ResultActivity.this);



//        final Handler handle = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                super.handleMessage(msg);
//                progressDialog.incrementProgressBy(1);
//            }
//        };
        String Testfile_name;
        Testfile_name=Reportname.substring(0, Reportname.length() - 3);
        Testfile_name=Testfile_name+"txt";
//        File logFile = new File(this.getExternalFilesDir(null), "TestFile.txt");
        File logFile = new File(this.getExternalFilesDir(null), Testfile_name);
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
//                System.out.println(logFile.getCanonicalPath());
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
//            progressDialog.setMax(100);
//            progressDialog.setMessage("Please Wait...");
//            progressDialog.setTitle("Copying contents to file..");
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            progressDialog.show();
//            handle.sendMessage(handle.obtainMessage());

            buf.append(text);

            buf.newLine();
            buf.close();
            MediaScannerConnection.scanFile(this,
                    new String[]{logFile.toString()},
                    null,
                    null);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            Log.e("ReadWriteFile", "Unable to write to the TestFile.txt file.");
            e.printStackTrace();
        }

        finally {

        }
    }

    public static double[] makeCumul(double[] in) {
        double[] out = new double[in.length];
        double total = 0;
        for (int i = 0; i < in.length; i++) {
            total += in[i];
            out[i] = total;
        }
        return out;
    }

}
