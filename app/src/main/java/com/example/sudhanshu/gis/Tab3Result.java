package com.example.sudhanshu.gis;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.google.gson.Gson;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.sudhanshu.gis.ResultActivity.verifyStoragePermissions;
import static com.example.sudhanshu.gis.StartActivity.getDifferenceDays;

public class Tab3Result extends Fragment {
    private static String[] PERMISSIONS_STORAGE = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    String Reportname;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final String farmer_name=getActivity().getIntent().getExtras().getString("farmername");
        final String circle_name=getActivity().getIntent().getExtras().getString("circlename");
        final String year_selected=getActivity().getIntent().getExtras().getString("year");
        final String irrigation_amount=getActivity().getIntent().getExtras().getString("irrigation_amount");
        final String per_irrigation_water=getActivity().getIntent().getExtras().getString("per_irrigation_water");
        final String irrigation_dates=getActivity().getIntent().getExtras().getString("irrigation_dates");
        final Double yield = Double.parseDouble(getActivity().getIntent().getExtras().getString("yield")) * 0.0247105; //converting to kg/m2;
        double[] et = getActivity().getIntent().getExtras().getDoubleArray("et");
        String[] watering_computed = getActivity().getIntent().getExtras().getStringArray("watering_computed");
        int sow_offset=getActivity().getIntent().getIntExtra("sowing_offset",0);

//        final kharifModel km = (kharifModel)getActivity().getIntent().getSerializableExtra("serialize data");
        // Create SharedPreferences object.
        Context ctx = getActivity().getApplicationContext();
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("km_obj", MODE_PRIVATE);

        // Get saved string data in it.
        String kmstr = sharedPreferences.getString("km_obj_serial", "");

        // Create Gson object and translate the json string to related java object array.
        Gson gson = new Gson();
        final kharifModel km = gson.fromJson(kmstr, kharifModel.class);

        boolean isChecked = getActivity().getIntent().getExtras().getBoolean("checkBoxValue", false);
        Reportname=org.apache.commons.lang3.StringUtils.capitalize(farmer_name)+"_"+km.district+"_"+km.crop+".pdf";
        int crop_end_index = km.crop_end_index -1;
        String[] xaxes = new String[crop_end_index+1];
        ArrayList<Entry> aet = new ArrayList<>();
        ArrayList<Entry> pet = new ArrayList<>();
        ArrayList<Entry> sm = new ArrayList<>();
        ArrayList<Entry> runoff = new ArrayList<>();
        ArrayList<BarEntry> rainfall = new ArrayList<>();
        ArrayList<BarEntry> irrigation = new ArrayList<>();
        ArrayList<Entry> vulnerability = new ArrayList<>();

        ArrayList<Entry> ground_water_recharge = new ArrayList<>();


        ArrayList<Entry> rain_s = new ArrayList<>();
        ArrayList<Entry> runoff_s = new ArrayList<>();
        ArrayList<Entry> gwr_s = new ArrayList<>();

        List<Double> pet_val = km.pet;
        List <Double> aet_val =km.aet;
        double vuln_sum=0;
        double rain_sum=0;
        double runoff_sum=0;
        double gwr_sum=0;
        final Date date = new Date();
        final View rootView3 = inflater.inflate(R.layout.tab_3, container, false);
        final View rootview = container.getChildAt(0);
        final View rootview2 = container.getChildAt(1);

        BootstrapButton savebtn = (BootstrapButton) rootView3.findViewById(R.id.savebtn1);


        if(isChecked)
        {
            for (int i =0;i<=crop_end_index;i++){
                aet.add(new Entry(i,km.budget.aet[i]));

                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));

                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                xaxes[i]=Integer.toString(i);


                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i]);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);



                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];

                gwr_sum += km.budget.GW_rech[i];


                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));

            }


            Toast.makeText(getContext(), "Daily Values added to text file", Toast.LENGTH_SHORT).show();
        }else{
            for (int i =0;i<=crop_end_index;i++){
                aet.add(new Entry(i,km.budget.aet[i]));
                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));
                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                xaxes[i]=Integer.toString(i);
                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i]);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);
                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];
                gwr_sum += km.budget.GW_rech[i];
                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));

            }
        }
        String dtt = km.year_sel+"-06-01";// Start date
//        Toast.makeText(this, ""+dtt, Toast.LENGTH_SHORT).show();
        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cc = Calendar.getInstance();
        try {
            cc.setTime(sdff.parse(dtt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cc.add(Calendar.DATE, sow_offset);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        String output = sdf1.format(cc.getTime());



        TextView watering_tv= ( TextView )rootView3.findViewById(R.id.watering_computed);
        String watering_="[ ";
        for (String w: watering_computed)
            watering_ += w+", ";
        watering_tv.setText("Watering "+ watering_+" ]");

        TextView Sowing_datetxt= ( TextView )rootView3.findViewById(R.id.Sowing_date1);
        Sowing_datetxt.setText("पेरणीची तारीखः "+ output);
        TextView irrigation_datetxt = (TextView) rootView3.findViewById(R.id.irrigation_dates);
        irrigation_datetxt.setText("सिंचनाची तारीख "+irrigation_dates);
        TextView textView = (TextView) rootView3.findViewById(R.id.Starting_Soil_Moisture1);
//        Starting_Soil_Moisture.setText("Starting Soil Moisture: "+ (Math.round(km.starting_soil_moisture * 1000))+ " mm");


        //Original Values

//        TextView Raintxt = (TextView) findViewById(R.id.rainfall);
//        Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
//        TextView runofftxt = (TextView) findViewById(R.id.runoff);
//        runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");
//        TextView aettxt = (TextView) findViewById(R.id.aet);
//        aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
//        TextView smtxt = (TextView) findViewById(R.id.soil_moist);
//        smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
//        TextView gwrtxt = (TextView) findViewById(R.id.gwr);
//        gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
//        TextView defecittxt = (TextView) findViewById(R.id.defecit);
//        defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");



        TextView dry_spells = (TextView) rootView3.findViewById(R.id.dry_spells1);
        Calendar c = Calendar.getInstance();
        String dry_spell_report = new String ();
        int count =0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        for(int key:km.budget.dry_spells.keySet()){
            c.set(Integer.parseInt(km.year_sel),05,01);
            c.add(Calendar.DATE,key);
            String start_date = sdf.format(c.getTime());
            int indx1 = (int)getDifferenceDays(start_date,year_selected);
            long milis1 = c.getTimeInMillis();
            c.add(Calendar.DATE,km.budget.dry_spells.get(key));
            long milis2 = c.getTimeInMillis();
            String end_date = sdf.format(c.getTime());
            int indx2 = (int)getDifferenceDays(end_date, year_selected);
            long cumulative_aet_pet_diff = 0;
            for(int i=indx1;i<=indx2;i++){
                double diff = km.pet.get(i)-km.aet.get(i);
                cumulative_aet_pet_diff += diff;
            }
            count+=1;
            long diff_days = (milis2-milis1)/(24 * 60 * 60 * 1000);
            dry_spell_report += "पाण्याचा खंड "+count+":" + start_date +" पासून "+ end_date+" पर्यंत :"+" ["+diff_days+"-दिवसांची एकूण तूट"+cumulative_aet_pet_diff +" mm]"+"\n";
//            Log.d("Dry spell",Integer.toString(key));
        }
        dry_spells.setText(dry_spell_report);


        // Original Values

//        TextView Raintxt_CE = (TextView) findViewById(R.id.rainfall_ce);
//        Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
//        TextView runofftxt_CE = (TextView) findViewById(R.id.runoff_ce);
//        runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
//        TextView aettxt_CE = (TextView) findViewById(R.id.aet_ce);
//        aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
//        TextView smtxt_CE = (TextView) findViewById(R.id.soil_moist_ce);
//        smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
//        TextView gwrtxt_CE = (TextView) findViewById(R.id.gwr_ce);
//        gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
//        TextView defecittxt_CE = (TextView) findViewById(R.id.defecit_ce);
//        defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");

        int selected = getActivity().getIntent().getIntExtra("selected",1);



        TextView row1 = (TextView) rootView3.findViewById(R.id.textView1_1);
        TextView row2 = (TextView) rootView3.findViewById(R.id.textView2_1);
        TextView row3 = (TextView) rootView3.findViewById(R.id.textView3_1);
        TextView row4 = (TextView) rootView3.findViewById(R.id.textView4_1);
        TextView row5 = (TextView) rootView3.findViewById(R.id.textView5_1);
        TextView row6 = (TextView) rootView3.findViewById(R.id.textView6_1);
        TextView row7 = (TextView) rootView3.findViewById(R.id.textView7_1);
        TextView row7_1_1 = (TextView) rootView3.findViewById(R.id.textView7_1_1);
        TextView row8 = (TextView) rootView3.findViewById(R.id.textView8_1);
        TextView row8_1_1 = (TextView) rootView3.findViewById(R.id.textView8_1_1);
        TextView row9 = (TextView) rootView3.findViewById(R.id.textView9_1);
        TextView row9_1_1 = (TextView) rootView3.findViewById(R.id.textView9_1_1);
        TextView row10 = (TextView) rootView3.findViewById(R.id.textView10_1);
        TextView row11 = (TextView) rootView3.findViewById(R.id.textView11_1);
        TextView row12 = (TextView) rootView3.findViewById(R.id.textView12_1);

        TextView row13 = (TextView) rootView3.findViewById(R.id.textView13_1);
        TextView row14 = (TextView) rootView3.findViewById(R.id.textView14_1);
        TextView row15 = (TextView) rootView3.findViewById(R.id.textView15_1);
        TextView row16 = (TextView) rootView3.findViewById(R.id.textView16_1);
        TextView row17 = (TextView) rootView3.findViewById(R.id.textView17_1);
        TextView row18 = (TextView) rootView3.findViewById(R.id.textView18_1);
        TextView row19 = (TextView) rootView3.findViewById(R.id.textView19_1);
        TextView row20 = (TextView) rootView3.findViewById(R.id.textView20_1);
        TextView row21 = (TextView) rootView3.findViewById(R.id.textView21_1);

        TextView row22 = (TextView) rootView3.findViewById(R.id.textView22_1);
        TextView row23 = (TextView) rootView3.findViewById(R.id.textView23_1);
        TextView row24 = (TextView) rootView3.findViewById(R.id.textView24_1);






        row1.setText("परिमापक");
        row2.setText("मान्सून समाप्ती ");
        row3.setText("पीक समाप्ती ");
        row4.setText("पाऊस");
        row5.setText((Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
        row6.setText((Math.round(km.budget.rain_sum_crop_end))+ " mm");
        row7.setText("अपधाव (Runoff) ");
        row7_1_1.setText("पीकाला पाण्याची गरज");
        row8_1_1.setText((Math.round(km.budget.pet_monsoon_end))+" mm");
        row9_1_1.setText((Math.round(km.budget.pet_crop_end))+" mm");
        row8.setText((Math.round(km.budget.runoff_monsoon_end))+ " mm");
        row9.setText((Math.round(km.budget.runoff_crop_end))+ " mm");
        // row10.setText("एकूण पीक एईटी");
        row10.setText("पिकाने घेतलेले पाणी");
        row11.setText((Math.round(km.budget.aet_monsoon_end))+ " mm");
        row12.setText((Math.round(km.budget.aet_crop_end))+ " mm");



        row13.setText("मातीचा ओलावा");
        row14.setText(Math.round(km.budget.sm_monsoon_end)+ " mm" );
        row15.setText(Math.round(km.budget.sm_crop_end)+ " mm");
        row16.setText("भूजल भरणा/रिचार्ज");
        row17.setText((Math.round(km.budget.gwr_monsoon_end)+ " mm"));
        row18.setText((Math.round(km.budget.gwr_crop_end)+ " mm"));
        row19.setText("एकूण तूट");
        row20.setText((Math.round(km.budget.defecit_sum_monsoon))+ " mm");
        row21.setText((Math.round(km.budget.defecit_sum_crop))+ " mm");

        row22.setText("Water Productivity(kg/m3)");
        row23.setText(String.valueOf(Math.round(yield*100.0/(km.budget.aet_monsoon_end/1000))/100.0));
        row24.setText(String.valueOf(Math.round(yield*100.0/(km.budget.aet_crop_end/1000))/100.0));


        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyStoragePermissions(getActivity());
                CombinedChart saveChart = (CombinedChart) rootview.findViewById(R.id.chart_1);
                saveChart.saveToGallery("chart",50);

                CombinedChart saveChart1 = (CombinedChart) rootview2.findViewById(R.id.chart_2);
                saveChart1.saveToGallery("chart1",50);

//
//                Document doc = new Document();
//                try {
//                    PdfWriter.getInstance(doc, new FileOutputStream("ImageDemo.pdf"));
//                    doc.open();
//
//                    // Creating image by file name
//                    String filename = "chart.jpg";
//                    Image image = Image.getInstance(filename);
//                    doc.add(image);
//
//                    // The following line to prevent the "Server returned
//                    // HTTP response code: 403" error.
//                    System.setProperty("http.agent", "Chrome");
//
//                    // Creating image from a URL
//                    String url = "chart1.jpg";
//                    image = Image.getInstance(url);
//                    doc.add(image);
//                    doc.close();
//                } catch (DocumentException | IOException e) {
//                    e.printStackTrace();
//                } finally {
////                    doc.close();
//                }


                Document document = new Document();

                try {
//                     Reportname=org.apache.commons.lang3.StringUtils.capitalize(farmer_name)+"_"+km.district+"_"+km.crop+".pdf";
                    File f = new File(Environment.getExternalStorageDirectory(), Reportname);
                    PdfWriter.getInstance(document, new FileOutputStream(f));
                    document.open();
                    Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);


                    document.add(new Paragraph("                                       Farm Based Water Balance",boldFont));
                    document.add(new Paragraph("                              Project:-PoCRA,Government of Maharashtra,IIT Bombay-2019"));
                    document.add(new Paragraph("                         For any queries or comments please contact us at pocra@cse.iitb.ac.in"));


                    document.add(new Paragraph("  "));
                    document.add(new Paragraph(String.valueOf(date)));
                    document.add(new Paragraph("--------------------------------------------------------"));
                    document.add(new Paragraph("Farmer : " +farmer_name));
                    document.add(new Paragraph("Year : " +year_selected));
                    document.add(new Paragraph("District : " +km.district));
                    document.add(new Paragraph("Circle : " +circle_name));
                    document.add(new Paragraph("Lattitude : " +km.lat));
                    document.add(new Paragraph("Longitude : " +km.logni));

                    document.add(new Paragraph("Crop : " +km.crop));
                    document.add(new Paragraph("Soil Type : " +km.soil_type));
                    document.add(new Paragraph("Depth Value :" +String.valueOf(km.depth_value)));
                    document.add(new Paragraph("Per_irrigation_water:  "+ per_irrigation_water));
                    document.add(new Paragraph("Irrigation_amount:  "+ irrigation_amount));
                    document.add(new Paragraph("Irrigation_dates:  "+ irrigation_dates));
                    document.add(new Paragraph("  "));
                    document.add(new Paragraph("  "));

                    document.add(new Paragraph("--------------------------------------------------------"));

//                    document.add(new Paragraph("Monsoon End Values:-",boldFont));
//
//                    document.add(new Paragraph("--------------------------------------------------------"));
//
//                    TextView Raintxt = (TextView) findViewById(R.id.rainfall);
//                    document.add(new Paragraph((String) Raintxt.getText()));
////                    Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
//                    TextView runofftxt = (TextView) findViewById(R.id.runoff);
//                    document.add(new Paragraph((String) runofftxt.getText()));
////                    runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");
//
//                    TextView aettxt = (TextView) findViewById(R.id.aet);
//                    document.add(new Paragraph((String) aettxt.getText()));
////                    aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
//                    TextView smtxt = (TextView) findViewById(R.id.soil_moist);
//                    document.add(new Paragraph((String) smtxt.getText()));
////                    smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
//                    TextView gwrtxt = (TextView) findViewById(R.id.gwr);
//                    document.add(new Paragraph((String) gwrtxt.getText()));
////                    gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
//                    TextView defecittxt = (TextView) findViewById(R.id.defecit);
//                    document.add(new Paragraph((String) defecittxt.getText()));
////                    defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");
//                    TextView dry_spells = (TextView) findViewById(R.id.dry_spells);
//                    document.add(new Paragraph((String) dry_spells.getText()));
//                    document.add(new Paragraph("  "));
//                    document.add(new Paragraph("  "));
//
//
//                    document.add(new Paragraph("#######################################################"));
//                    document.add(new Paragraph("  "));
//                    document.add(new Paragraph("  "));
//
//
//                    document.add(new Paragraph("Crop End Values:-",boldFont));
//
//                    document.add(new Paragraph("--------------------------------------------------------"));
//
//                    TextView Raintxt_CE = (TextView) findViewById(R.id.rainfall_ce);
//                    document.add(new Paragraph((String) Raintxt_CE.getText()));
//
////                    Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
//                    TextView runofftxt_CE = (TextView) findViewById(R.id.runoff_ce);
//                    document.add(new Paragraph((String) runofftxt_CE.getText()));
////                    runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
//                    TextView aettxt_CE = (TextView) findViewById(R.id.aet_ce);
//                    document.add(new Paragraph((String) aettxt_CE.getText()));
////                    aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
//                    TextView smtxt_CE = (TextView) findViewById(R.id.soil_moist_ce);
//                    document.add(new Paragraph((String) smtxt_CE.getText()));
////                    smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
//                    TextView gwrtxt_CE = (TextView) findViewById(R.id.gwr_ce);
//                    document.add(new Paragraph((String) gwrtxt_CE.getText()));
////                    gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
//                    TextView defecittxt_CE = (TextView) findViewById(R.id.defecit_ce);
//                    document.add(new Paragraph((String) defecittxt_CE.getText()));
////                    defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");


                    document.add(new Paragraph("Monsoon End Values:-",boldFont));


                    document.add(new Paragraph("--------------------------------------------------------"));

                    TextView Raintxt = (TextView) rootView3.findViewById(R.id.textView5_1);
                    document.add(new Paragraph("Rainfall in Monsoon: "+(String) Raintxt.getText()));
//                    Raintxt.setText("Rainfall in Monsoon: "+ (Math.round(km.budget.rain_sum_monsoon_end))+ " mm");
                    TextView runofftxt = (TextView) rootView3.findViewById(R.id.textView8_1);
                    document.add(new Paragraph("Runoff in Monsoon: "+ (String) runofftxt.getText()));
//                    runofftxt.setText("Runoff in Monsoon: "+ (Math.round(km.budget.runoff_monsoon_end))+ " mm");

                    TextView aettxt = (TextView) rootView3.findViewById(R.id.textView11_1);
                    document.add(new Paragraph("Total crop AET in Monsoon: "+(String) aettxt.getText()));
//                    aettxt.setText("Total crop AET in Monsoon: "+(Math.round(km.budget.aet_monsoon_end)));
                    TextView smtxt = (TextView) rootView3.findViewById(R.id.textView14_1);
                    document.add(new Paragraph("Soil Moisture at Monsoon End: "+(String) smtxt.getText()));
//                    smtxt.setText("Soil Moisture at Monsoon End: "+ Math.round(km.budget.sm_monsoon_end) );
                    TextView gwrtxt = (TextView) rootView3.findViewById(R.id.textView17_1);
                    document.add(new Paragraph("GW Recarge in Monsoon: "+ (String) gwrtxt.getText()));
//                    gwrtxt.setText("GW Recarge in Monsoon: "+ (Math.round(km.budget.gwr_monsoon_end)+ " mm"));
                    TextView defecittxt = (TextView) rootView3.findViewById(R.id.textView20_1);
                    document.add(new Paragraph("Total Deficit in Monsoon: "+ (String) defecittxt.getText()));
//                    defecittxt.setText("Total Defecit in Monsoon: "+ (Math.round(km.budget.defecit_sum_monsoon))+ " mm");
                    TextView water_prod_monsoon_end = rootView3.findViewById(R.id.textView23_1);
                    document.add(new Paragraph("Water Productivity monsoon end(kg/m3): "+water_prod_monsoon_end.getText()));
                    TextView water_prod_crop_end = rootView3.findViewById(R.id.textView24_1);
                    document.add(new Paragraph("Water Productivity monsoon end(kg/m3): "+water_prod_crop_end.getText()));

                    document.add(new Paragraph("  "));


                    document.add(new Paragraph("#######################################################"));
                    document.add(new Paragraph("  "));
                    document.add(new Paragraph("  "));


                    document.add(new Paragraph("Crop End Values:-",boldFont));

                    document.add(new Paragraph("--------------------------------------------------------"));

                    TextView Raintxt_CE = (TextView) rootView3.findViewById(R.id.textView6_1);
                    document.add(new Paragraph("Rainfall in CE: "+(String) Raintxt_CE.getText()));

//                    Raintxt_CE.setText("Rainfall in CE: "+ (Math.round(km.budget.rain_sum_crop_end))+ " mm");
                    TextView runofftxt_CE = (TextView) rootView3.findViewById(R.id.textView9_1);
                    document.add(new Paragraph("Runoff in CE: "+(String) runofftxt_CE.getText()));
//                    runofftxt_CE.setText("Runoff in CE: "+ (Math.round(km.budget.runoff_crop_end))+ " mm");
                    TextView aettxt_CE = (TextView) rootView3.findViewById(R.id.textView12_1);
                    document.add(new Paragraph("Total crop AET in CE: "+(String) aettxt_CE.getText()));
//                    aettxt_CE.setText("Total crop AET in CE: "+(Math.round(km.budget.aet_crop_end)));
                    TextView smtxt_CE = (TextView) rootView3.findViewById(R.id.textView15_1);
                    document.add(new Paragraph("Soil Moisture at CE: "+ (String) smtxt_CE.getText()));
//                    smtxt_CE.setText("Soil Moisture at CE: "+ Math.round(km.budget.sm_crop_end) );
                    TextView gwrtxt_CE = (TextView) rootView3.findViewById(R.id.textView18_1);
                    document.add(new Paragraph("GW Recarge in CE: "+ (String) gwrtxt_CE.getText()));
//                    gwrtxt_CE.setText("GW Recarge in CE: "+ (Math.round(km.budget.gwr_crop_end)+ " mm"));
                    TextView defecittxt_CE = (TextView) rootView3.findViewById(R.id.textView21_1);
                    document.add(new Paragraph("Total Deficit in CE: "+(String) defecittxt_CE.getText()));
//                    defecittxt_CE.setText("Total Defecit in CE: "+ (Math.round(km.budget.defecit_sum_crop))+ " mm");

                    document.newPage();



                    TextView wb = (TextView) rootview.findViewById(R.id.wb1);
                    document.add(new Paragraph((String) wb.getText(),boldFont));

                    document.add(new Paragraph("  "));


                    TextView daily = (TextView) rootview.findViewById(R.id.daily1);
                    document.add(new Paragraph(String.valueOf(daily.getText())));

                    Image image = Image.getInstance("/storage/emulated/0/DCIM/chart.jpg");
                    image.scalePercent(50f);
                    document.add(image);

                    document.newPage();

                    TextView cumulative = (TextView) rootview2.findViewById(R.id.cumulative1);
                    document.add(new Paragraph(String.valueOf(cumulative.getText())));

                    image = Image.getInstance("/storage/emulated/0/DCIM/chart1.jpg");
                    image.scalePercent(50f);
                    document.add(image);

                    document.newPage();






//                    wb.setText(wb.getText()+km.crop+" at ("+km.lat.substring(0,Math.min(km.lat.length(),5)) +", "+km.logni.substring(0,Math.min(km.logni.length(),5))+")");


//                    daily.setText(daily.getText()+" for crop "+ km.crop);
//                    cumulative.setText(cumulative.getText()+" for crop "+ km.crop);


//                    appendLog(date.toString());

                    document.close();

                    Toast.makeText(getContext(), "Report Generated", Toast.LENGTH_SHORT).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }

        });


        return rootView3;





    }
}