package com.example.sudhanshu.gis;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.support.v4.math.MathUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class kharifModel implements Serializable {
    String district;
    double depth_value;
    double ksat;
    double sat;
    double wilting_point;
    double field_capacity;
    double sm1;
    double sm1before;
    double sm2;
    double cn;
    String lulc;
    String soil_type;
    double slope;
    String crop;
    double layer2_moisture;
    double sm1_fraction;
    String hsg;
    double WP_depth;
    double Smax;
    double w2;
    double w1;
    double daily_perc_factor;
    double depletion_factor;
    double sm1_before;
    double sm2_before;
    double r_to_second_layer;
    int crop_end_index;
    double [] rainfall;
    double [] irrgation;
    double [] drip_sm;
    Lookup l;
    Summerize budget;
    List <Double> sm,runoff,infil,aet,sec_run_off,GW_rech,pet;
    int sowing_date_offset=0;
    double starting_soil_moisture=0;
    int monsoon_end_index;

    String year_sel, lat, logni;


    @SuppressLint("LongLogTag")
    kharifModel(String lulc, String soil_type, double depth, double slope, String crop, double[] rf, double[] irrg, int monsoon_end_index, String year_sel, String lat, String logni, String district_db, double[] smoisture){
        l = new Lookup();
        depth_value=depth;
        Log.d("Value of slope in km is ", String.valueOf(slope));
        this.slope = slope;
        this.crop=crop;
        Log.d("Soil type ", soil_type);
        ksat=l.soil.get(soil_type).ksat;
        wilting_point = l.soil.get(soil_type).wilting_point;
        field_capacity = l .soil.get(soil_type).field_capacity;
        sat = l.soil.get(soil_type).saturation;
        sm1_fraction=layer2_moisture=wilting_point;
        this.lulc =lulc;
        this.soil_type=soil_type;
        hsg = l.soil.get(soil_type).HSG;
        sm = new ArrayList <Double>();
        runoff = new ArrayList <Double>();
        infil = new ArrayList <Double>();
        aet = new ArrayList <Double>();
        sec_run_off = new ArrayList <Double>();
        GW_rech = new ArrayList <Double>();
        pet = new ArrayList <Double>();
        rainfall = Arrays.copyOf(rf,rf.length);
        irrgation = Arrays.copyOf(irrg,irrg.length);
        drip_sm = Arrays.copyOf(smoisture, smoisture.length);
        this.monsoon_end_index = monsoon_end_index;
        this.year_sel = year_sel;
        this.lat =lat;
        this.logni = logni;
        this.district=district_db;

        Log.d("Start Soil Moisture" , String.valueOf(sm1_fraction * depth_value));
        starting_soil_moisture=sm1_fraction * depth_value;


//        =sowing_date_offset;

//        Log.d("Data to Kharif",lulc+"\t"+soil_type+"\t"+Double.toString(depth)+"\t"+Double.toString(slope)+"\t"+crop);




    }


    void setup(boolean hourly){
        double sat_depth = sat* depth_value* 1000;
        WP_depth = wilting_point* depth_value* 1000;
        double FC_depth = field_capacity* depth_value* 1000;
        double root_depth = l.crop.get(crop).root;
        depletion_factor = l.crop.get(crop).depletion;

        if(depth_value<=root_depth){
            sm1=depth_value - 0.05;
            sm2=0.05;
        }
        else{
            sm1=root_depth;
            sm2=depth_value-root_depth;

        }
        double cn_val;
        if(hsg == "A")
            cn_val = l.cn.get(lulc).a;
        else if(hsg == "B")
            cn_val = l.cn.get(lulc).b;
        else if (hsg =="C")
            cn_val = l.cn.get(lulc).c;
        else
            cn_val = l.cn.get(lulc).d;
        // switch (hsg)
        // {
        //   case "A": cn_val = l.cn.get(l.landuse.get(lulc)).a;
        //             break;
        //   case "B": cn_val = l.cn.get(l.landuse.get(lulc)).b;
        //             break;
        //   case "C": cn_val = l.cn.get(l.landuse.get(lulc)).c;
        //             break;
        //   case "D": cn_val = l.cn.get(l.landuse.get(lulc)).d;
        //             break;
        // }


        double cn3 = cn_val * Math.exp(0.00673*(100-cn_val));
        double cn3_s,cn_s,cn1_s;
        if(slope>5.0){
            cn_s = (((cn3-cn_val)/3)*(1-2*Math.exp(-13.86*slope*0.01))) + cn_val;
        }
        else{
            cn_s = cn_val;
        }
        cn1_s = cn_s - 20*(100-cn_s)/(100-cn_s+Math.exp(2.533-0.0636*(100-cn_s)));
        cn3_s = cn_s *Math.exp(0.00673*(100-cn_s));
        Smax = 25.4 * (1000/cn1_s - 10);
        double s3 = 25.4 * (1000/cn3_s - 10);
        w2 = (Math.log((FC_depth- WP_depth)/(1- s3/Smax) - (FC_depth - WP_depth )) - Math.log ((sat_depth - WP_depth)/(1-2.54/Smax) - (sat_depth - WP_depth)))/((sat_depth- WP_depth) - (FC_depth - WP_depth));
        w1 = Math.log((FC_depth- WP_depth)/(1- s3/Smax) - (FC_depth - WP_depth)) + w2 * (FC_depth -WP_depth);
        double TT_perc = (sat_depth- FC_depth)/ ksat;
        int num_daily_phases;
        if(hourly)
            num_daily_phases=24;
        else
            num_daily_phases=1;

        daily_perc_factor = 1 - Math.exp(-24 /num_daily_phases / TT_perc) ;

    }

    void primary_runoff(int day, int hour, double [] rain){
        sm.add(  (sm1_fraction * sm1 + layer2_moisture * sm2) * 1000);
//        double sw = sm.get(sm.size() - 1) - WP_depth;
        double sw = ((sm1_fraction * sm1 + layer2_moisture * sm2 - WP_depth/1000) * 1000);
        double S_swat = Smax *(1 - sw/(sw + Math.exp(w1 - w2 * sw)));
        double Cn_swat = 25400/(S_swat+254);
        double Ia_swat = 0.2 * S_swat;
        int rain_idx;
        if(hour != -1){
            rain_idx = 24*day+hour;
        }else{
            rain_idx = day;
        }

        if(rain[rain_idx] > Ia_swat) {
            runoff.add(Math.pow((rain[rain_idx] - Ia_swat), 2) / (rain[rain_idx] + 0.8 * S_swat));
        }else
            runoff.add(0.0);
        infil.add(rain[rain_idx] - runoff.get(rain_idx));

    }

    void Aet (int day, int hour){
        double ks;
        if(sm1_fraction < wilting_point){
            ks=0;
        }
        else if (sm1_fraction > (field_capacity *(1- depletion_factor) + depletion_factor * wilting_point))
        {
            ks =1;
        }
        else{
            ks =  (sm1_fraction - wilting_point)/(field_capacity - wilting_point) /(1- depletion_factor);

        }

        if(hour != -1)
            aet.add(ks * pet.get(24*day+hour) );
        else
            aet.add(ks * pet.get(day) );

    }

    void percolation_below_root_zone(int day, int hour, double soilmoisture){

        int idx;
        if(hour != -1)
            idx = 24*day+hour;
        else
            idx = day;
        sm1_before = (sm1_fraction*sm1+soilmoisture/1000 +((infil.get(idx) - aet.get(idx))/1000))/sm1;
        if(sm1_before<field_capacity){
            r_to_second_layer =0;
        }
        else if(layer2_moisture<sat){
            r_to_second_layer = Math.min((sat - layer2_moisture) * sm2 * 1000,(sm1_before - field_capacity) * sm1 * 1000 * daily_perc_factor);
        }
        else{
            r_to_second_layer=0;
        }
        sm2_before = (layer2_moisture*sm2*1000 + r_to_second_layer)/sm2/1000;

    }

    void secondary_runoff(int day,int hr){
        double candidate_new_sm1_frac = (sm1_before*sm1-r_to_second_layer/1000)/sm1;
        double candidate_sec_runoff = (candidate_new_sm1_frac - sat)*sm1*1000;
        sec_run_off.add(Math.max(candidate_sec_runoff, 0));
        sm1_fraction = Math.min(candidate_new_sm1_frac, sat);

    }

    void percolation_to_GW(int day,  int hr){
        GW_rech.add(Math.max((sm2_before - field_capacity)*sm2*daily_perc_factor*1000,0));
        layer2_moisture = Math.min(((sm2_before*sm2*1000- GW_rech.get(GW_rech.size()-1))/sm2/1000),sat);

    }


    void pET_calculation(double [] et0, int sowing_threshold,double [] rain,int monsoon_start_index, boolean hourly){
        double rain_sum=0;
        int dry_spell = monsoon_start_index;
        sowing_date_offset=dry_spell;
        List<Double> kc = new ArrayList<Double>(l.crop.get(crop).list);

        for (int i=0;i<=dry_spell;i++){
            kc.add(0,0.0);
        }
        int remain=0;
        remain = 365-kc.size();
        if(remain>0)
            crop_end_index = kc.size();
        else
            crop_end_index = 365;
        for(int k=0; k<remain ; k++){
            kc.add(0.0);
        }
        while (kc.size()>365)
            kc.remove(kc.size()-1);

        if(hourly){
            for (int i=0;i<24*365;i++){
                pet.add(et0[i]*kc.get(i/24));
            }

        }else{
            for (int i=0;i<365;i++){
                pet.add(et0[i]*kc.get(i));
            }
        }

    }

    public void summerize(boolean hourly)
    {
        if(hourly){
            //sum up all the hourly outputs to transform them into daily outputs
            List <Double> sm_,runoff_,infil_,aet_,sec_run_off_,GW_rech_,pet_;
            sm_ = new ArrayList<Double>();
            runoff_ = new ArrayList<Double>();
            infil_ = new ArrayList<Double>();
            aet_ = new ArrayList<Double>();
            sec_run_off_ = new ArrayList<Double>();
            GW_rech_ = new ArrayList<Double>();
            pet_ = new ArrayList<Double>();
            double [] rainfall_ = new double[365];

            for(int i=0;i<365;i++){
                Double runoff_day=0.0;
                for (Double j: this.runoff.subList(24*i,24*(i+1)))
                    runoff_day+=j;
                runoff_.add(runoff_day);
//                Double sm_day=0.0;
//                for(Double j: this.sm.subList(24*i,24*(i+1)))
//                    sm_day+=j;
                sm_.add(this.sm.get(24*i));
                Double infil_day = 0.0;
                for(Double j: this.infil.subList(24*i, 24*(i+1)))
                    infil_day+=j;
                infil_.add(infil_day);
                Double aet_day=0.0;
                for(Double j: this.aet.subList(24*i,24*(i+1)))
                    aet_day+=j;
                aet_.add(aet_day);
                Double sec_run_off_day = 0.0;
                for(Double j: this.sec_run_off.subList(24*i, 24*(i+1)))
                    sec_run_off_day+=j;
                sec_run_off_.add(sec_run_off_day);
                Double GW_rech_day=0.0;
                for(Double j: this.GW_rech.subList(24*i, 24*(i+1)))
                    GW_rech_day+=j;
                GW_rech_.add(GW_rech_day);
                Double pet_day=0.0;
                for(Double j: this.pet.subList(24*i, 24*(i+1)))
                    pet_day+=j;
                pet_.add(pet_day);
                double rainfall_day = 0.0;
                for(double j: Arrays.copyOfRange(this.rainfall,24*i,24*(i+1)))
                    rainfall_day+=j;
                rainfall_[i] = rainfall_day;
            }
            this.sm = sm_;
            this.pet = pet_;
            this.GW_rech = GW_rech_;
            this.sec_run_off = sec_run_off_;
            this.aet = aet_;
            this.infil = infil_;
            this.runoff = runoff_;
            this.rainfall = rainfall_;

        }



//        Log.d("Error data infil", String.valueOf(sm.size()));
        budget = new Summerize(sm.size());
        for(int i=0;i<sm.size();i++){
            budget.sm[i]= sm.get(i).floatValue() - (float)WP_depth;
            budget.runoff[i] = runoff.get(i).floatValue() +  sec_run_off.get(i).floatValue();
            budget.infil[i] = infil.get(i).floatValue();

            budget.aet[i] = aet.get(i).floatValue();
            budget.pet[i] = pet.get(i).floatValue();
            budget.sec_run_off[i] = sec_run_off.get(i).floatValue();
            budget.GW_rech[i] = GW_rech.get(i).floatValue();
        }
        for(int i=0; i<=monsoon_end_index; i++){
            budget.infil_monsoon_end+= budget.infil[i];
            budget.runoff_monsoon_end += budget.runoff[i];
            budget.defecit_sum_monsoon += (budget.pet[i] - budget.aet[i]);
            budget.gwr_monsoon_end += budget.GW_rech[i];
            budget.rain_sum_monsoon_end += rainfall[i];
            budget.aet_monsoon_end += budget.aet[i];
            budget.pet_monsoon_end += budget.pet[i];
        }
        budget.sm_monsoon_end = budget.sm[monsoon_end_index];
        budget.dry_spell_offset=sowing_date_offset;

//        Log.d("Crop end index", String.valueOf(crop_end_index));
        for(int i=0; i<crop_end_index; i++){


            budget.infil_crop_end+= budget.infil[i];
            budget.runoff_crop_end += budget.runoff[i];
            budget.defecit_sum_crop += (budget.pet[i] - budget.aet[i]);
            budget.gwr_crop_end += budget.GW_rech[i];
            budget.rain_sum_crop_end += rainfall[i];
            budget.aet_crop_end += budget.aet[i];
            budget.pet_crop_end += budget.pet[i];
//            Log.d("infil_crop_end", String.valueOf(budget.infil_crop_end));
//            Log.d("runoff_crop_end", String.valueOf(budget.runoff_crop_end));
//            Log.d("deficit_sum_crop", String.valueOf(budget.defecit_sum_crop));
//            Log.d("gwr_crop_end", String.valueOf(budget.gwr_crop_end));
//            Log.d("rain_sum_crop_end", String.valueOf(budget.rain_sum_crop_end));
//            Log.d("aet_crop_end", String.valueOf(budget.aet_crop_end));
//            Log.d("Length of infil", String.valueOf(budget.infil.length));
//            Log.d("Length of sm", String.valueOf(budget.sm.length));
//            Log.d("Value of i iteration", String.valueOf(i));
        }
        budget.sm_crop_end = budget.sm[(crop_end_index-1)];
//        Log.d("infil_crop_end", String.valueOf(budget.infil_crop_end));
//        Log.d("runoff_crop_end", String.valueOf(budget.runoff_crop_end));
//        Log.d("deficit_sum_crop", String.valueOf(budget.defecit_sum_crop));
//        Log.d("gwr_crop_end", String.valueOf(budget.gwr_crop_end));
//        Log.d("rain_sum_crop_end", String.valueOf(budget.rain_sum_crop_end));
//        Log.d("aet_crop_end", String.valueOf(budget.aet_crop_end));
//        Log.d("budget.sm[CEI]", String.valueOf(budget.sm[crop_end_index]));
//        Log.d("Length of infil", String.valueOf(budget.infil.length));
//        Log.d("Length of sm", String.valueOf(budget.sm.length));


        int strt_index = -1, end_index = 0;
        for(int i=0;i<=monsoon_end_index;i++){
            if(rainfall[i]==0 ){
                if(strt_index==-1)
                    strt_index=i;
                end_index = i;
            }
            else{
                if(strt_index!=-1){
                    if(end_index-strt_index > 6)
                        budget.dry_spells.put(strt_index,end_index-strt_index);
                    strt_index=-1;

                }
            }
        }
        if(strt_index != -1 && (end_index - strt_index)>6)
            budget.dry_spells.put(strt_index,end_index-strt_index);




    }



}


class Summerize implements Serializable {
        float [] sm, runoff, infil, aet, pet, sec_run_off, GW_rech ;
        int dry_spell_offset;
        float sm_monsoon_end, infil_monsoon_end, runoff_monsoon_end,defecit_sum_monsoon,gwr_monsoon_end, rain_sum_monsoon_end, aet_monsoon_end, pet_monsoon_end;
        float sm_crop_end, infil_crop_end, runoff_crop_end,defecit_sum_crop,gwr_crop_end, rain_sum_crop_end, aet_crop_end, pet_crop_end;
        TreeMap <Integer,Integer> dry_spells;

    public Summerize(int size) {
        sm  = new float[size];
        runoff = new float[size];
        infil = new float[size];
        aet = new float[size];
        pet = new float[size];
        sec_run_off= new float[size];
        GW_rech = new float[size];
        dry_spells = new TreeMap<>();
        sm_monsoon_end=0;
        infil_monsoon_end=0;
        runoff_monsoon_end=0;
        defecit_sum_monsoon=0;
        gwr_monsoon_end=0;
        rain_sum_monsoon_end=0;
        aet_monsoon_end = 0;
        pet_monsoon_end = 0;
        pet_crop_end = 0;

        sm_crop_end=0;
        infil_crop_end=0;
        runoff_crop_end=0;
        defecit_sum_crop=0;
        gwr_crop_end=0;
        rain_sum_crop_end=0;
        aet_crop_end = 0;
        dry_spell_offset=0;
    }


}