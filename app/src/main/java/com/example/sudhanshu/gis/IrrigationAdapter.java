package com.example.sudhanshu.gis;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.AwesomeTextView;
import com.beardedhen.androidbootstrap.BootstrapDropDown;
import com.beardedhen.androidbootstrap.BootstrapDropDown.OnDropDownItemClickListener;
import com.beardedhen.androidbootstrap.BootstrapEditText;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class IrrigationAdapter extends Adapter<IrrigationAdapter.ViewHolder> {
    /* access modifiers changed from: private */
    public Context context;
    String[] irrigation_type = {"Sprinkler", "Flood", "Drip", "Furrow", "Other"};
    private List<ListItem> listItems;

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        public BootstrapDropDown type;
        public BootstrapEditText water;
        public BootstrapEditText water_hours;
        public BootstrapEditText area;
        public BootstrapEditText spacing_along;
        public BootstrapEditText spacing_between;
        public BootstrapEditText num_of_doys;
        public  AwesomeTextView computed_watering;

        public ViewHolder(View itemView) {
            super(itemView);
            this.water = (BootstrapEditText) itemView.findViewById(R.id.water);
            this.type = (BootstrapDropDown) itemView.findViewById(R.id.type);
            this.water_hours = itemView.findViewById(R.id.water_hours);
            this.area = itemView.findViewById(R.id.total_area);
            this.spacing_along = itemView.findViewById(R.id.spacing_along);
            this.spacing_between = itemView.findViewById(R.id.spacing_between);
            this.num_of_doys = itemView.findViewById(R.id.water_days);
            this.computed_watering = itemView.findViewById(R.id.computed_watering);
        }
    }

    public IrrigationAdapter(List<ListItem> listItems2, Context context2) {
        this.listItems = listItems2;
        this.context = context2;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_edit, parent, false));
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    public void calculateAndSetWatering(final ViewHolder holder, final int position){
        String irrigation_type = holder.type.getText().toString();
        String spacing_al = holder.spacing_along.getText().toString();
        String spacing_bet = holder.spacing_between.getText().toString();
        String water = holder.water.getText().toString();
        String water_hrs = holder.water_hours.getText().toString();
        Double irr_watering;
        if(!isNumeric(spacing_al) || !isNumeric(spacing_bet) || !isNumeric(water) || !isNumeric(water_hrs)){
            if(isNumeric((water)))
                holder.computed_watering.setText("Computed Watering: "+Math.round(Double.parseDouble(water)*100.0)/100.0+" mm");
            return;
        }

        if(irrigation_type.equals("Drip")){
            Double spacing_along = Double.parseDouble(spacing_al);
            Double spacing_between = Double.parseDouble(spacing_bet);
            double num_of_drippers = 4046 / (spacing_along*spacing_between);
            irr_watering = (num_of_drippers * Double.parseDouble(water) * Double.parseDouble(water_hrs) * 0.9)/4046;
        }else if (irrigation_type.equals("Sprinkler")){
            Double spacing_along = Double.parseDouble(spacing_al);
            Double spacing_between = Double.parseDouble(spacing_bet);
            double num_of_sprinklers = 4046 / (spacing_along*spacing_between);
            irr_watering = (num_of_sprinklers * Double.parseDouble(water)*Double.parseDouble(water_hrs)*0.75)/4046;
        }else {
            irr_watering = Double.parseDouble(water)*Double.parseDouble(water_hrs)/4046;
        }
        holder.computed_watering.setText("Computed Watering: "+Math.round(irr_watering*100.0)/100.0+" mm");





    }

    public void afterChange(final ViewHolder holder, Editable editable, final int position){
        Editor edit = IrrigationAdapter.this.context.getSharedPreferences("dates", 0).edit();
        StringBuilder sb = new StringBuilder();
        sb.append("Water_");
        sb.append(Integer.toString(position));
        edit.putString(sb.toString(), holder.water.getText().toString());

        StringBuilder sb3 = new StringBuilder();
        sb3.append("Waterhours_");
        sb3.append(Integer.toString(position));
        edit.putString(sb3.toString(), holder.water_hours.getText().toString());

        StringBuilder sb2 = new StringBuilder();
        sb2.append("Type_");
        sb2.append(Integer.toString(position));
        edit.putString(sb2.toString(), holder.type.getText().toString());

        StringBuilder sb5 = new StringBuilder();
        sb5.append("Area_");
        sb5.append(Integer.toString(position));
        try {
            edit.putString(sb5.toString(), holder.area.getText().toString());
        }
        catch (Exception e){
            edit.putString(sb5.toString(), "");
        }

        StringBuilder sb6 = new StringBuilder();
        sb6.append("SpacingAlong_");
        sb6.append(Integer.toString(position));
        try {
            edit.putString(sb6.toString(), holder.spacing_along.getText().toString());
        }
        catch (Exception e){
            edit.putString(sb6.toString(), "");
        }

        StringBuilder sb7 = new StringBuilder();
        sb7.append("SpacingBetween_");
        sb7.append(Integer.toString(position));
        try {
            edit.putString(sb7.toString(), holder.spacing_between.getText().toString());
        }
        catch (Exception e){
            edit.putString(sb7.toString(), "");
        }

        StringBuilder sb8 = new StringBuilder();
        sb8.append("Days_");
        sb8.append(Integer.toString(position));
        try {
            edit.putString(sb8.toString(), holder.num_of_doys.getText().toString());
        }
        catch (Exception e){
            edit.putString(sb8.toString(), "");
        }


        edit.apply();
        calculateAndSetWatering(holder, position);

    }

    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ListItem listItem = (ListItem) this.listItems.get(position);
//        holder.water.setText("40");
        holder.water.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
               afterChange(holder, editable, position);
            }
        });

//        holder.water_hours.setText("3");
        holder.water_hours.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
               afterChange(holder, editable, position);
            }
        });


        holder.type.setDropdownData(this.irrigation_type);
        holder.type.setOnDropDownItemClickListener(new OnDropDownItemClickListener() {
            public void onItemClick(ViewGroup parent, View v, int id) {
                holder.type.setText(IrrigationAdapter.this.irrigation_type[id]);
                Log.d("holder_type", holder.type.getText().toString());
                if(!(holder.type.getText().toString().equals("Drip") || holder.type.getText().toString().equals("Sprinkler"))){
                    holder.spacing_along.setVisibility(View.INVISIBLE);
                    holder.spacing_between.setVisibility(View.INVISIBLE);
                }else{
                    holder.spacing_along.setVisibility(View.VISIBLE);
                    holder.spacing_between.setVisibility(View.VISIBLE);
                }
                afterChange(holder,null, position);
            }
        });


//        holder.area.setText("0");
        holder.area.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                afterChange(holder, editable,position);
            }
        });
//        holder.num_of_doys.setText("1");
        holder.num_of_doys.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                afterChange(holder, editable, position);
            }
        });

//        holder.spacing_between.setText("0");
        holder.spacing_between.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                afterChange(holder, editable, position);
            }
        });

        holder.spacing_along.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                afterChange(holder, editable, position);
            }
        });


//        Editor edit = this.context.getSharedPreferences("dates", 0).edit();
//        StringBuilder sb = new StringBuilder();
//        sb.append("Water_");
//        sb.append(Integer.toString(position));
//        edit.putString(sb.toString(), holder.water.getText().toString());
//        StringBuilder sb2 = new StringBuilder();
//
//        StringBuilder sb3 = new StringBuilder();
//        sb3.append("Waterhours_");
//        sb3.append(Integer.toString(position));
//        edit.putString(sb3.toString(), holder.water_hours.getText().toString());
//
//        sb2.append("Type_");
//        sb2.append(Integer.toString(position));
//        edit.putString(sb2.toString(), holder.type.getText().toString());
//        edit.apply();
        afterChange(holder, null, position);
    }

    public int getItemCount() {
        return this.listItems.size();
    }
}