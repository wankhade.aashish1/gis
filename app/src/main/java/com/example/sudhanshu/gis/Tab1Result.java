package com.example.sudhanshu.gis;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import static android.content.Context.MODE_PRIVATE;

public class Tab1Result extends Fragment {
    private static String[] PERMISSIONS_STORAGE = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    String Reportname;
    CombinedChart combinedChart;
    CombinedChart combinedChart1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("tab1res","called");
        final String farmer_name=getActivity().getIntent().getExtras().getString("farmername");
        final String circle_name=getActivity().getIntent().getExtras().getString("circlename");
        final String year_selected=getActivity().getIntent().getExtras().getString("year");
        final String irrigation_amount=getActivity().getIntent().getExtras().getString("irrigation_amount");
        final String per_irrigation_water=getActivity().getIntent().getExtras().getString("per_irrigation_water");
        final String irrigation_dates=getActivity().getIntent().getExtras().getString("irrigation_dates");
        final String crop_sel=getActivity().getIntent().getExtras().getString("crop_name");
        final String lat=getActivity().getIntent().getExtras().getString("lat");
        final String longi=getActivity().getIntent().getExtras().getString("longi");
        double[] et = getActivity().getIntent().getExtras().getDoubleArray("et");
        int sow_offset=getActivity().getIntent().getIntExtra("sowing_offset",0);

//        final kharifModel km = (kharifModel)getActivity().getIntent().getSerializableExtra("serialize data");
        // Create SharedPreferences object.
        Context ctx = getActivity().getApplicationContext();
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("km_obj", MODE_PRIVATE);

        // Get saved string data in it.
        String kmstr = sharedPreferences.getString("km_obj_serial", "");

        // Create Gson object and translate the json string to related java object array.
        Gson gson = new Gson();
        final kharifModel km = gson.fromJson(kmstr, kharifModel.class);

        boolean isChecked = getActivity().getIntent().getExtras().getBoolean("checkBoxValue", false);
        Reportname=org.apache.commons.lang3.StringUtils.capitalize(farmer_name)+"_"+km.district+"_"+km.crop+".pdf";
        View rootView = inflater.inflate(R.layout.tab_1, container, false);
        combinedChart = (CombinedChart) rootView.findViewById(R.id.chart_1);
        final int crop_end_index = km.crop_end_index -1;
        String[] xaxes = new String[(crop_end_index+1)];
        ArrayList<Entry> aet = new ArrayList<>();
        ArrayList<Entry> pet = new ArrayList<>();
        ArrayList<Entry> sm = new ArrayList<>();
        ArrayList<Entry> runoff = new ArrayList<>();
        ArrayList<BarEntry> rainfall = new ArrayList<>();
        ArrayList<BarEntry> irrigation = new ArrayList<>();
        ArrayList<BarEntry> smoisture = new ArrayList<>();
        ArrayList<Entry> vulnerability = new ArrayList<>();

        ArrayList<Entry> ground_water_recharge = new ArrayList<>();


        ArrayList<Entry> rain_s = new ArrayList<>();
        ArrayList<Entry> runoff_s = new ArrayList<>();
        ArrayList<Entry> gwr_s = new ArrayList<>();

        List<Double> pet_val = km.pet;
        List <Double> aet_val =km.aet;

        double vuln_sum=0;
        double rain_sum=0;
        double runoff_sum=0;
        double gwr_sum=0;

        if(isChecked)
        {
            for (int i =0;i<=crop_end_index;i++){
                aet.add(new Entry(i,km.budget.aet[i]));

                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));

                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                smoisture.add(new BarEntry(i,(float) km.drip_sm[i]));
                xaxes[i]=Integer.toString(i);
                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i]);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i]);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);
                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];
                gwr_sum += km.budget.GW_rech[i];
                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));
            }


            Toast.makeText(getContext(), "Daily Values added to text file", Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (int i =0;i<=crop_end_index;i ++){
                aet.add(new Entry(i,km.budget.aet[i]));

                ground_water_recharge.add(new Entry(i,km.budget.GW_rech[i]));

                pet.add(new Entry(i,km.budget.pet[i]));
                sm.add(new Entry (i,km.budget.sm[i] ));
                runoff.add(new Entry (i,km.budget.runoff[i]));
                rainfall.add(new BarEntry(i, (float)km.rainfall[i]+ (float)km.irrgation[i]));
                irrigation.add(new BarEntry(i, (float)km.irrgation[i]));
                smoisture.add(new BarEntry(i,(float) km.drip_sm[i]));
                xaxes[i]=Integer.toString(i);
                String aet_f="Day "+i+" AET "+Double.toString(aet_val.get(i));
                String pet_f="Day "+i+" PET " +Double.toString(pet_val.get(i));
                String SM_f="Day "+i+" SM " +Double.toString(km.budget.sm[i]);
                String Runoff_f="Day "+i+" Runoff "+ Double.toString(km.budget.runoff[i]);
                String Rainfall_f="Day "+i+" Rainfall "+ Double.toString((float)km.rainfall[i]);
                String Irrigation_f="Day "+i+" Irrigation "+ Double.toString((float)km.irrgation[i/24]);
                String RainIrri_f="Day "+i+" Rainfall+Irrigation "+ Double.toString((float)km.rainfall[i]+ (float)km.irrgation[i/24]);
                String RainIrri_gwr="Day "+i+" Ground Water Recharge "+ Double.toString((float)km.budget.GW_rech[i]);
                vuln_sum += km.budget.pet[i] - km.budget.aet[i];
                rain_sum += km.rainfall[i];
                runoff_sum+=km.budget.runoff[i];
                gwr_sum += km.budget.GW_rech[i];
                vulnerability.add(new Entry(i,(float)vuln_sum));
                rain_s.add(new Entry(i,(float)rain_sum));
                runoff_s.add(new Entry(i, (float) runoff_sum));
                gwr_s.add(new Entry(i, (float) gwr_sum));

            }

        }

        TextView cn = rootView.findViewById(R.id.wb1);
        cn.setText(cn.getText().toString()+" \nपीक: "+crop_sel+" \nlat: "+lat+" \nlong: "+longi);
        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();
        LineDataSet lineDataSet1 = new LineDataSet(aet,"AET");
        lineDataSet1.setDrawCircles(false);
        lineDataSet1.setColor(Color.BLACK);
        lineDataSet1.setLineWidth(1);
        LineDataSet lineDataSet2 = new LineDataSet(pet,"PET");
        lineDataSet2.setDrawCircles(false);
        lineDataSet2.setColor(Color.RED);
        lineDataSet2.setLineWidth(1);
        BarDataSet barDataSet5 = new BarDataSet(rainfall,"Rainfall");
        barDataSet5.setColor(Color.BLUE);
        BarDataSet barDataSet6 = new BarDataSet(irrigation,"Irrigation");
        barDataSet6.setColor(Color.GREEN);
        BarDataSet barDataSet7 = new BarDataSet(smoisture, "Drip");
        barDataSet7.setColor(Color.YELLOW);
        BarData bd5 =new BarData();
        bd5.addDataSet(barDataSet5);
        bd5.addDataSet(barDataSet6);
        bd5.addDataSet(barDataSet7);
        lineDataSets.add(lineDataSet1);
        lineDataSets.add(lineDataSet2);
        CombinedData combinedData = new CombinedData();
        combinedData.setData(new LineData(lineDataSets));
        combinedData.setData(bd5);
        XAxis xAxis = combinedChart.getXAxis();
        xAxis.setPosition(XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0.0f);
        xAxis.setAxisMaximum((float) crop_end_index);
        xAxis.setGranularity(1.0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            public String getFormattedValue(float value, AxisBase axis) {
                String[] mdays = new String[(crop_end_index + 1)];
                Calendar cal = Calendar.getInstance();
                cal.set(Integer.parseInt(year_selected), 6, 1);
                int hour=0;
                for (int i = 0; i <=crop_end_index; i++) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.valueOf(cal.get(Calendar.DATE)));
                    sb.append("/");
                    sb.append(String.valueOf(cal.get(Calendar.MONTH)));
                    mdays[i] = sb.toString();
                    cal.add(Calendar.DATE, 1);

                }
                return mdays[(int) value];
            }
        });

        combinedChart.setData(combinedData);
        Description des1 = combinedChart.getDescription();
        des1.setText("Daily values                                                                          ");
        return rootView;
    }
}
